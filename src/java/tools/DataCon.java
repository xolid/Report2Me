/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tools;

import java.sql.*;

/**
 *
 * @author czarate
 */
public class DataCon {
    
    private Connection con;
    private PreparedStatement stmt;
    private static final String HOST="localhost";
    private static final String DB="report2me";
    private static final String USER="root";
    private static final String PASS="Nemesis0";
    public enum Parametro { STRING,INT,DOUBLE };
    
    public DataCon() throws ClassNotFoundException, SQLException{
        Class.forName("com.mysql.jdbc.Driver");
        con=DriverManager.getConnection("jdbc:mysql://"+HOST+"/"+DB,USER,PASS);
    }
    
    public int executeUpdate(String query,Object[][] params) throws SQLException{
        stmt = con.prepareStatement(query);
        addParameters(params);
        return stmt.executeUpdate();
    }
    
    public ResultSet executeQuery(String query,Object[][] params) throws SQLException{
        stmt = con.prepareStatement(query);
        addParameters(params);
        return stmt.executeQuery();
    }
    
    private void addParameters(Object[][] params) throws SQLException{
        stmt.clearParameters();
        int index = 1;
        if(params!=null){
            for (Object[] param : params) {
                if (param[0] == Parametro.STRING) {
                    if(String.valueOf(param[1]).equals("null")){
                        stmt.setNull(index,java.sql.Types.VARCHAR);
                    }else{
                        stmt.setString(index, String.valueOf(param[1]));
                    }
                }
                if (param[0] == Parametro.INT) {
                    if(String.valueOf(param[1]).equals("null")){
                        stmt.setNull(index, java.sql.Types.INTEGER);
                    }else{
                        stmt.setInt(index, Integer.valueOf(String.valueOf(param[1])));
                    }
                }
                if (param[0] == Parametro.DOUBLE) {
                    if(String.valueOf(param[1]).equals("null")){
                        stmt.setNull(index, java.sql.Types.DOUBLE);
                    }else{
                        stmt.setDouble(index, Double.valueOf(String.valueOf(param[1])));
                    }
                }
                index++;
            }
        }
    }
    
    public Connection getConnection(){
        return con;
    }
    
    public void closeConnection() throws SQLException{
        if(stmt!=null){
            if(!stmt.isClosed()){
                stmt.close();
            }
        }
        if(con!=null){
            if(!con.isClosed()){
                con.close();
            }
        }
    }
    
    @Override
    protected void finalize() throws SQLException, Throwable{
        closeConnection();
        super.finalize();
    }
     
}
