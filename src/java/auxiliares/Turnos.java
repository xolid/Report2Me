/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package auxiliares;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author czarate
 */
public class Turnos {
    
    private final static Object[][] items={{0,"Matutino"},{1,"Vespertino"},{2,"Nocturno"}};
    private final List<Turno> turnos=new ArrayList();
    
    public Turnos(){
        for (Object[] item : items) {
            Turno turno=new Turno();
            turno.setIndice((Integer) item[0]);
            turno.setNombre((String) item[1]);
            turnos.add(turno);
        }
    }
    
    public List<Turno> getTurnos(){
        return turnos;
    }
    
    public String getTurnoName(int index){
        return turnos.get(index).getNombre();
    }
    
}
