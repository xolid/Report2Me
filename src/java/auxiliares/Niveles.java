/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package auxiliares;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author czarate
 */
public class Niveles {
    
    private final static Object[][] items={{0,"Consultante Externo"},{1,"Capturista de Informacion"},{2,"Manejador del Sistema"},{3,"Administrador"}};
    private final List<Nivel> niveles=new ArrayList();
    
    public Niveles(){
        for (Object[] item : items) {
            Nivel nivel=new Nivel();
            nivel.setIndice((Integer) item[0]);
            nivel.setNombre((String) item[1]);
            niveles.add(nivel);
        }
    }
    
    public List<Nivel> getNiveles(){
        return niveles;
    }
    
    public String getNivelName(int index){
        return niveles.get(index).getNombre();
    }
    
}
