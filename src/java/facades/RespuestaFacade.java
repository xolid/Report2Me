/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Respuesta;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class RespuestaFacade {
    
    private final DataCon con;
    
    public RespuestaFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarRespuesta(Respuesta respuesta) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(respuesta.getIdrespuesta()==0){
            return con.executeUpdate(respuesta.insertMe(),respuesta.insertMeParameters());
        }else{
            return con.executeUpdate(respuesta.updateMe(),respuesta.updateMeParameters());
        }
    }
    
    public int borrarRespuesta(Respuesta respuesta) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(respuesta.deleteMe(),respuesta.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Respuesta> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Respuesta respuesta=new Respuesta();
            respuesta.loadMe(rs);
            list.add(respuesta);
        }
        return list;
    }
    
    private Respuesta toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Respuesta respuesta=new Respuesta();
        while(rs.next()){  
            respuesta.loadMe(rs);
        }
        return respuesta;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public int deleteAllbyInscrito(int inscrito_idinscrito) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,inscrito_idinscrito}};
        return con.executeUpdate("DELETE FROM respuesta WHERE inscrito_idinscrito = ?;",params);
    }
    
    public List<Respuesta> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM respuesta;",null));
    }
    
    public List<Respuesta> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM respuesta LIMIT "+index+","+size+";",null));
    }
    
    public List<Respuesta> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM respuesta "+(new Respuesta().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM respuesta;",null),"total");
    }

    public Respuesta getById(int idrespuesta) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idrespuesta}};
        return toClass(con.executeQuery("SELECT * FROM respuesta WHERE idrespuesta = ?;",params));
    }
    
    public List<Respuesta> getByInscrito(int inscrito_idinscrito) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,inscrito_idinscrito}};
        return toList(con.executeQuery("SELECT * FROM respuesta WHERE inscrito_idinscrito = ?;",params));
    }
    
    public Respuesta getByInscritoPregunta(int inscrito_idinscrito,int pregunta_idpregunta) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,inscrito_idinscrito},{DataCon.Parametro.INT,pregunta_idpregunta}};
        return toClass(con.executeQuery("SELECT * FROM respuesta WHERE inscrito_idinscrito = ? AND pregunta_idpregunta = ?;",params));
    }
    
}
