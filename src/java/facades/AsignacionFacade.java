/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Asignacion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class AsignacionFacade {
    
    private final DataCon con;
    
    public AsignacionFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarAsignacion(Asignacion asignacion) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(asignacion.getIdasignacion()==0){
            return con.executeUpdate(asignacion.insertMe(),asignacion.insertMeParameters());
        }else{
            return con.executeUpdate(asignacion.updateMe(),asignacion.updateMeParameters());
        }
    }
    
    public int borrarAsignacion(Asignacion asignacion) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(asignacion.deleteMe(),asignacion.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Asignacion> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Asignacion asignacion=new Asignacion();
            asignacion.loadMe(rs);
            list.add(asignacion);
        }
        return list;
    }
    
    private Asignacion toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Asignacion asignacion=new Asignacion();
        while(rs.next()){  
            asignacion.loadMe(rs);
        }
        return asignacion;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Asignacion> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM asignacion;",null));
    }
    
    public List<Asignacion> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM asignacion LIMIT "+index+","+size+";",null));
    }
    
    public List<Asignacion> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM asignacion "+(new Asignacion().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
        
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM asignacion;",null),"total");
    }

    public Asignacion getById(int idasignacion) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idasignacion}};
        return toClass(con.executeQuery("SELECT * FROM asignacion WHERE idasignacion = ?;",params));
    }
    
    public Asignacion getByUsuario(int usuario_idusuario) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,usuario_idusuario}};
        return toClass(con.executeQuery("SELECT * FROM asignacion WHERE usuario_idusuario = ?;",params));
    }
    
}
