/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Inscrito;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class InscritoFacade {
    
    private final DataCon con;
    
    public InscritoFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarInscrito(Inscrito inscrito) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(inscrito.getIdinscrito()==0){
            return con.executeUpdate(inscrito.insertMe(),inscrito.insertMeParameters());
        }else{
            return con.executeUpdate(inscrito.updateMe(),inscrito.updateMeParameters());
        }
    }
    
    public int borrarInscrito(Inscrito inscrito) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(inscrito.deleteMe(),inscrito.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Inscrito> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Inscrito inscrito=new Inscrito();
            inscrito.loadMe(rs);
            list.add(inscrito);
        }
        return list;
    }
    
    private Inscrito toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Inscrito inscrito=new Inscrito();
        while(rs.next()){  
            inscrito.loadMe(rs);
        }
        return inscrito;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Inscrito> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM inscrito;",null));
    }
        
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM inscrito;",null),"total");
    }

    public Inscrito getById(int idinscrito) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idinscrito}};
        return toClass(con.executeQuery("SELECT * FROM inscrito WHERE idinscrito = ?;",params));
    }
    
    public List<Inscrito> getByParams(int examen_idexamen,int escuela_idescuela,int grado_idgrado,int grupo_idgrupo) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,examen_idexamen},{DataCon.Parametro.INT,escuela_idescuela},{DataCon.Parametro.INT,grado_idgrado},{DataCon.Parametro.INT,grupo_idgrupo}};
        return toList(con.executeQuery("SELECT * FROM inscrito WHERE examen_idexamen = ? AND escuela_idescuela = ? AND grado_idgrado = ? AND grupo_idgrupo = ?;",params));
    }
    
    public Object getContestadas(int idinscrito) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idinscrito}};
        return toObject(con.executeQuery("SELECT Count(respuesta.idrespuesta) AS total FROM inscrito INNER JOIN respuesta ON respuesta.inscrito_idinscrito = inscrito.idinscrito WHERE inscrito.idinscrito = ?;",params),"total");
    }
    
}
