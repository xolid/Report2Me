/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Escuela;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class EscuelaFacade {
    
    private final DataCon con;
    
    public EscuelaFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarEscuela(Escuela escuela) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(escuela.getIdescuela()==0){
            return con.executeUpdate(escuela.insertMe(),escuela.insertMeParameters());
        }else{
            return con.executeUpdate(escuela.updateMe(),escuela.updateMeParameters());
        }
    }
    
    public int borrarEscuela(Escuela escuela) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(escuela.deleteMe(),escuela.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Escuela> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Escuela escuela=new Escuela();
            escuela.loadMe(rs);
            list.add(escuela);
        }
        return list;
    }
    
    private Escuela toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Escuela escuela=new Escuela();
        while(rs.next()){  
            escuela.loadMe(rs);
        }
        return escuela;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Escuela> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM escuela ORDER BY nombre;",null));
    }
    
    public List<Escuela> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM escuela ORDER BY nombre LIMIT "+index+","+size+";",null));
    }
    
    public List<Escuela> getBySupervision(int supervision_idsupervision) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,supervision_idsupervision}};
        return toList(con.executeQuery("SELECT * FROM escuela WHERE supervision_idsupervision = ? ORDER BY nombre;",params));
    }
    
    public List<Escuela> getByAsignacionUsuario(int usuario_idusuario) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,usuario_idusuario}};
        return toList(con.executeQuery("SELECT escuela.idescuela,escuela.nombre,escuela.direccion,escuela.telefono,escuela.centrotrabajo,escuela.responsable,escuela.turno,escuela.supervision_idsupervision FROM escuela INNER JOIN asignacion ON escuela.idescuela = asignacion.escuela_idescuela WHERE asignacion.usuario_idusuario = ?;",params));
    }
    
    public List<Escuela> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM escuela "+(new Escuela().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM escuela ORDER BY nombre;",null),"total");
    }

    public Escuela getById(int idescuela) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idescuela}};
        return toClass(con.executeQuery("SELECT * FROM escuela WHERE idescuela = ?;",params));
    }
    
}
