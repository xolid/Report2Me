/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Examen;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class ExamenFacade {
    
    private final DataCon con;
    
    public ExamenFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarExamen(Examen examen) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(examen.getIdexamen()==0){
            return con.executeUpdate(examen.insertMe(),examen.insertMeParameters());
        }else{
            return con.executeUpdate(examen.updateMe(),examen.updateMeParameters());
        }
    }
    
    public int borrarExamen(Examen examen) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(examen.deleteMe(),examen.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Examen> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Examen examen=new Examen();
            examen.loadMe(rs);
            list.add(examen);
        }
        return list;
    }
    
    private Examen toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Examen examen=new Examen();
        while(rs.next()){  
            examen.loadMe(rs);
        }
        return examen;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Examen> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM examen ORDER BY nombre;",null));
    }
    
    public List<Examen> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM examen ORDER BY nombre LIMIT "+index+","+size+";",null));
    }
    
    public List<Examen> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM examen "+(new Examen().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM examen ORDER BY nombre;",null),"total");
    }

    public Examen getById(int idexamen) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idexamen}};
        return toClass(con.executeQuery("SELECT * FROM examen WHERE idexamen = ?;",params));
    }
    
    public Object getTotalPreguntas(int idexamen, int idgrado) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idgrado},{DataCon.Parametro.INT,idexamen}};
        return toObject(con.executeQuery("SELECT Count(pregunta.idpregunta) AS total FROM examen INNER JOIN evaluacion ON evaluacion.examen_idexamen = examen.idexamen INNER JOIN pregunta ON pregunta.evaluacion_idevaluacion = evaluacion.idevaluacion INNER JOIN grado ON evaluacion.grado_idgrado = grado.idgrado WHERE grado.idgrado = ? AND examen.idexamen = ?;",params),"total");
    }
    
}
