/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Grupo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class GrupoFacade {
    
    private final DataCon con;
    
    public GrupoFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarGrupo(Grupo grupo) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(grupo.getIdgrupo()==0){
            return con.executeUpdate(grupo.insertMe(),grupo.insertMeParameters());
        }else{
            return con.executeUpdate(grupo.updateMe(),grupo.updateMeParameters());
        }
    }
    
    public int borrarGrupo(Grupo grupo) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(grupo.deleteMe(),grupo.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Grupo> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Grupo grupo=new Grupo();
            grupo.loadMe(rs);
            list.add(grupo);
        }
        return list;
    }
    
    private Grupo toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Grupo grupo=new Grupo();
        while(rs.next()){  
            grupo.loadMe(rs);
        }
        return grupo;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Grupo> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM grupo ORDER BY nombre;",null));
    }
    
    public List<Grupo> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM grupo ORDER BY nombre LIMIT "+index+","+size+";",null));
    }
    
    public List<Grupo> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM grupo "+(new Grupo().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM grupo ORDER BY nombre;",null),"total");
    }

    public Grupo getById(int idgrupo) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idgrupo}};
        return toClass(con.executeQuery("SELECT * FROM grupo WHERE idgrupo = ?;",params));
    }
    
}
