/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;
import tools.DataCon.Parametro;

/**
 *
 * @author czarate
 */
public class UsuarioFacade {
    
    private final DataCon con;
    
    public UsuarioFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarUsuario(Usuario usuario) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(usuario.getIdusuario()==0){
            return con.executeUpdate(usuario.insertMe(),usuario.insertMeParameters());
        }else{
            return con.executeUpdate(usuario.updateMe(),usuario.updateMeParameters());
        }
    }
    
    public int borrarUsuario(Usuario usuario) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(usuario.deleteMe(),usuario.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Usuario> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Usuario usuario=new Usuario();
            usuario.loadMe(rs);
            list.add(usuario);
        }
        return list;
    }
    
    private Usuario toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        Usuario usuario=new Usuario();
        while(rs.next()){  
            usuario.loadMe(rs);
        }
        return usuario;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Usuario> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM usuario ORDER BY nombre;",null));
    }
    
    public List<Usuario> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM usuario ORDER BY nombre LIMIT "+index+","+size+";",null));
    }
    
    public List<Usuario> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM usuario "+(new Usuario().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM usuario ORDER BY nombre;",null),"total");
    }

    public Usuario getById(int idusuario) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{Parametro.INT,idusuario}};
        return toClass(con.executeQuery("SELECT * FROM usuario WHERE idusuario = ?;",params));
    }
    
    public Usuario getByMailPass(String mail,String pass) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{Parametro.STRING,mail},{Parametro.STRING,pass}};
        return toClass(con.executeQuery("SELECT * FROM usuario WHERE mail = ? AND pass = ? AND fegreso IS NULL;",params));
    }
    
    public List<Usuario> getByNivel(int nivel) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{Parametro.INT,nivel}};
        return toList(con.executeQuery("SELECT * FROM usuario WHERE nivel = ?;",params));
    }
    
}
