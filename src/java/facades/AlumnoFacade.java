/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Alumno;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class AlumnoFacade {
    
    private final DataCon con;
    
    public AlumnoFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarAlumno(Alumno alumno) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(alumno.getIdalumno()==0){
            return con.executeUpdate(alumno.insertMe(),alumno.insertMeParameters());
        }else{
            return con.executeUpdate(alumno.updateMe(),alumno.updateMeParameters());
        }
    }
    
    public int borrarAlumno(Alumno alumno) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(alumno.deleteMe(),alumno.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Alumno> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Alumno alumno=new Alumno();
            alumno.loadMe(rs);
            list.add(alumno);
        }
        return list;
    }
    
    private Alumno toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Alumno alumno=new Alumno();
        while(rs.next()){  
            alumno.loadMe(rs);
        }
        return alumno;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public int deleteNOInscritos() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return con.executeUpdate("DELETE FROM alumno WHERE idalumno IN ( SELECT idalumno FROM ( SELECT idalumno FROM alumno AS t1 LEFT JOIN inscrito AS t2 ON t1.idalumno = t2.alumno_idalumno WHERE t2.alumno_idalumno IS NULL ) AS temp);",null);
    }
    
    public List<Alumno> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM alumno ORDER BY paterno;",null));
    }
    
    public List<Alumno> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM alumno ORDER BY paterno LIMIT "+index+","+size+";",null));
    }
    
    public List<Alumno> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM alumno "+(new Alumno().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM alumno;",null),"total");
    }

    public Alumno getById(int idalumno) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idalumno}};
        return toClass(con.executeQuery("SELECT * FROM alumno WHERE idalumno = ?;",params));
    }
    
    public Alumno getByCurp(String curp) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.STRING,curp}};
        return toClass(con.executeQuery("SELECT * FROM alumno WHERE curp = ?;",params));
    }
    
    public List<Alumno> getNOInscritos() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("select idalumno, curp, nombre, paterno, fnacimiento from inscrito right outer join alumno on inscrito.alumno_idalumno = alumno.idalumno where idinscrito is NULL;",null));
    }
    
    public List<Alumno> getNoInscritosLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("select idalumno, curp, nombre, paterno, materno, fnacimiento from inscrito right outer join alumno on inscrito.alumno_idalumno = alumno.idalumno where idinscrito is NULL "+(new Alumno().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getNoInscritosTotal() throws SQLException {
        return toObject(con.executeQuery("select count(*) AS total from inscrito right outer join alumno on inscrito.alumno_idalumno = alumno.idalumno where idinscrito is NULL;",null),"total");
    }
    
}
