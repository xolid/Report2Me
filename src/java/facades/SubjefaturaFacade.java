/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Subjefatura;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class SubjefaturaFacade {
    
    private final DataCon con;
    
    public SubjefaturaFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarSubjefatura(Subjefatura subjefatura) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(subjefatura.getIdsubjefatura()==0){
            return con.executeUpdate(subjefatura.insertMe(),subjefatura.insertMeParameters());
        }else{
            return con.executeUpdate(subjefatura.updateMe(),subjefatura.updateMeParameters());
        }
    }
    
    public int borrarSubjefatura(Subjefatura subjefatura) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(subjefatura.deleteMe(),subjefatura.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Subjefatura> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Subjefatura subjefatura=new Subjefatura();
            subjefatura.loadMe(rs);
            list.add(subjefatura);
        }
        return list;
    }
    
    private Subjefatura toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Subjefatura subjefatura=new Subjefatura();
        while(rs.next()){  
            subjefatura.loadMe(rs);
        }
        return subjefatura;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Subjefatura> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM subjefatura ORDER BY nombre;",null));
    }
    
    public List<Subjefatura> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM subjefatura ORDER BY nombre LIMIT "+index+","+size+";",null));
    }
    
    public List<Subjefatura> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM subjefatura "+(new Subjefatura().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM subjefatura ORDER BY nombre;",null),"total");
    }

    public Subjefatura getById(int idsubjefatura) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idsubjefatura}};
        return toClass(con.executeQuery("SELECT * FROM subjefatura WHERE idsubjefatura = ?;",params));
    }
    
}
