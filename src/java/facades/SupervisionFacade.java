/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Supervision;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class SupervisionFacade {
    
    private final DataCon con;
    
    public SupervisionFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarSupervision(Supervision supervision) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(supervision.getIdsupervision()==0){
            return con.executeUpdate(supervision.insertMe(),supervision.insertMeParameters());
        }else{
            return con.executeUpdate(supervision.updateMe(),supervision.updateMeParameters());
        }
    }
    
    public int borrarSupervision(Supervision supervision) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(supervision.deleteMe(),supervision.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Supervision> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Supervision supervision=new Supervision();
            supervision.loadMe(rs);
            list.add(supervision);
        }
        return list;
    }
    
    private Supervision toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Supervision supervision=new Supervision();
        while(rs.next()){  
            supervision.loadMe(rs);
        }
        return supervision;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Supervision> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM supervision ORDER BY nombre;",null));
    }
    
    public List<Supervision> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM supervision ORDER BY nombre LIMIT "+index+","+size+";",null));
    }
    
    public List<Supervision> getBySubjefatura(int subjefatura_idsubjefatura) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,subjefatura_idsubjefatura}};
        return toList(con.executeQuery("SELECT * FROM supervision WHERE subjefatura_idsubjefatura = ? ORDER BY nombre;",params));
    }
    
    public List<Supervision> getBySubjefaturaLimited(int subjefatura_idsubjefatura,int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,subjefatura_idsubjefatura}};
        return toList(con.executeQuery("SELECT * FROM supervision WHERE subjefatura_idsubjefatura = ? ORDER BY nombre LIMIT "+index+","+size+";",params));
    }
    
    public List<Supervision> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM supervision "+(new Supervision().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM supervision ORDER BY nombre;",null),"total");
    }

    public Supervision getById(int idsupervision) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idsupervision}};
        return toClass(con.executeQuery("SELECT * FROM supervision WHERE idsupervision = ?;",params));
    }
    
}
