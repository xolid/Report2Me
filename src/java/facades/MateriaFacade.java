/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Materia;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class MateriaFacade {
    
    private final DataCon con;
    
    public MateriaFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarMateria(Materia materia) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(materia.getIdmateria()==0){
            return con.executeUpdate(materia.insertMe(),materia.insertMeParameters());
        }else{
            return con.executeUpdate(materia.updateMe(),materia.updateMeParameters());
        }
    }
    
    public int borrarMateria(Materia materia) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(materia.deleteMe(),materia.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Materia> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Materia materia=new Materia();
            materia.loadMe(rs);
            list.add(materia);
        }
        return list;
    }
    
    private Materia toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Materia materia=new Materia();
        while(rs.next()){  
            materia.loadMe(rs);
        }
        return materia;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Materia> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM materia ORDER BY nombre;",null));
    }
    
    public List<Materia> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM materia ORDER BY nombre LIMIT "+index+","+size+";",null));
    }
    
    public List<Materia> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM materia "+(new Materia().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM materia ORDER BY nombre;",null),"total");
    }

    public Materia getById(int idmateria) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idmateria}};
        return toClass(con.executeQuery("SELECT * FROM materia WHERE idmateria = ?;",params));
    }
    
}
