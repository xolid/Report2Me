/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Pregunta;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class PreguntaFacade {
    
    private final DataCon con;
    
    public PreguntaFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarPregunta(Pregunta pregunta) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(pregunta.getIdpregunta()==0){
            return con.executeUpdate(pregunta.insertMe(),pregunta.insertMeParameters());
        }else{
            return con.executeUpdate(pregunta.updateMe(),pregunta.updateMeParameters());
        }
    }
    
    public int borrarPregunta(Pregunta pregunta) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(pregunta.deleteMe(),pregunta.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Pregunta> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Pregunta pregunta=new Pregunta();
            pregunta.loadMe(rs);
            list.add(pregunta);
        }
        return list;
    }
    
    private Pregunta toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Pregunta pregunta=new Pregunta();
        while(rs.next()){  
            pregunta.loadMe(rs);
        }
        return pregunta;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Pregunta> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM pregunta ORDER BY indice;",null));
    }
    
    public List<Pregunta> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM pregunta LIMIT "+index+","+size+";",null));
    }
    
    public List<Pregunta> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM pregunta "+(new Pregunta().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM pregunta;",null),"total");
    }

    public Pregunta getById(int idpregunta) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idpregunta}};
        return toClass(con.executeQuery("SELECT * FROM pregunta WHERE idpregunta = ?;",params));
    }
    
    public List<Pregunta> getByEvaluacion(int evaluacion_idevaluacion) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,evaluacion_idevaluacion}};
        return toList(con.executeQuery("SELECT * FROM pregunta WHERE evaluacion_idevaluacion = ? ORDER BY indice;",params));
    }
    
}
