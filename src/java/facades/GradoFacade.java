/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import beans.Grado;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import tools.DataCon;

/**
 *
 * @author czarate
 */
public class GradoFacade {
    
    private final DataCon con;
    
    public GradoFacade() throws ClassNotFoundException, SQLException{
        con=new DataCon();
    }
    
    public int salvarGrado(Grado grado) throws SQLException, IllegalArgumentException, IllegalAccessException{
        if(grado.getIdgrado()==0){
            return con.executeUpdate(grado.insertMe(),grado.insertMeParameters());
        }else{
            return con.executeUpdate(grado.updateMe(),grado.updateMeParameters());
        }
    }
    
    public int borrarGrado(Grado grado) throws SQLException, IllegalArgumentException, IllegalAccessException{
        return con.executeUpdate(grado.deleteMe(),grado.deleteMeParameters());
    }
    
    @Override
    protected void finalize() throws Throwable{
        close();
        super.finalize();
    }
    
    public void close() throws SQLException{
        con.closeConnection();
    }
    
    private List<Grado> toList(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException{
        List list=new java.util.ArrayList();
        while(rs.next()){
            Grado grado=new Grado();
            grado.loadMe(rs);
            list.add(grado);
        }
        return list;
    }
    
    private Grado toClass(ResultSet rs) throws SQLException, IllegalArgumentException, IllegalAccessException, IllegalArgumentException, IllegalAccessException{
        Grado grado=new Grado();
        while(rs.next()){  
            grado.loadMe(rs);
        }
        return grado;
    }
    
    private Object toObject(ResultSet rs,String campo) throws SQLException{
        Object object=null;
        while(rs.next()){
            object=rs.getObject(campo);
        }
        return object;
    }
    
    public List<Grado> getAll() throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM grado ORDER BY nombre;",null));
    }
    
    public List<Grado> getAllLimited(int index,int size) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM grado ORDER BY nombre LIMIT "+index+","+size+";",null));
    }
    
    public List<Grado> getAllLimitedOrdered(int index,int size,String ordered) throws SQLException, IllegalArgumentException, IllegalAccessException {
        return toList(con.executeQuery("SELECT * FROM grado "+(new Grado().exist(ordered)?"ORDER BY "+ordered:"")+" LIMIT "+index+","+size+";",null));
    }
    
    public Object getAllTotal() throws SQLException {
        return toObject(con.executeQuery("SELECT COUNT(*) AS total FROM grado ORDER BY nombre;",null),"total");
    }

    public Grado getById(int idgrado) throws SQLException, IllegalArgumentException, IllegalAccessException {
        Object[][] params = {{DataCon.Parametro.INT,idgrado}};
        return toClass(con.executeQuery("SELECT * FROM grado WHERE idgrado = ?;",params));
    }
    
}
