/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Asignacion extends Bean{
    
    protected int idasignacion;
    protected int usuario_idusuario;
    protected int escuela_idescuela;

    /**
     * @return the idasignacion
     */
    public int getIdasignacion() {
        return idasignacion;
    }

    /**
     * @param idasignacion the idasignacion to set
     */
    public void setIdasignacion(int idasignacion) {
        this.idasignacion = idasignacion;
    }

    
    
    public static void main(String[] args){
        Asignacion asignacion = new Asignacion();
        System.out.println(asignacion.createMe());
    }

    /**
     * @return the usuario_idusuario
     */
    public int getUsuario_idusuario() {
        return usuario_idusuario;
    }

    /**
     * @param usuario_idusuario the usuario_idusuario to set
     */
    public void setUsuario_idusuario(int usuario_idusuario) {
        this.usuario_idusuario = usuario_idusuario;
    }

    /**
     * @return the escuela_idescuela
     */
    public int getEscuela_idescuela() {
        return escuela_idescuela;
    }

    /**
     * @param escuela_idescuela the escuela_idescuela to set
     */
    public void setEscuela_idescuela(int escuela_idescuela) {
        this.escuela_idescuela = escuela_idescuela;
    }
    
}
