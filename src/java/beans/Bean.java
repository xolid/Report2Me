/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;
import java.lang.reflect.Field;
import java.sql.SQLException;
import tools.DataCon.Parametro;

/**
 *
 * @author czarate
 */
public class Bean {
    
    public void loadMe(java.sql.ResultSet rs) throws IllegalArgumentException, IllegalAccessException, SQLException{
        Class c=this.getClass();
        for(Field f:c.getDeclaredFields()){
            if(f.getType().getName().equals("java.lang.String")){
                f.set(this,rs.getString(f.getName()));
            }
            if(f.getType().getName().equals("int")){
                f.set(this,rs.getInt(f.getName()));
            }
            if(f.getType().getName().equals("double")){
                f.set(this,rs.getDouble(f.getName()));
            }
        }
    }
    
    public String insertMe(){
        Class c=this.getClass();
        String tabla=c.getName().substring(c.getName().lastIndexOf(".")+1).toLowerCase();
        String campos="";
        String valores="";
        boolean getid=true;
        for(Field f:c.getDeclaredFields()){
            if(getid){
                getid=false;
            }else{
                if(campos.equals("")){
                    campos+=f.getName();
                }else{
                    campos+=","+f.getName();
                }
                if(valores.equals("")){
                    valores+="?";
                }else{
                    valores+=",?";
                }
            }
        }
        return "INSERT INTO "+tabla+"("+campos+") VALUES("+valores+")";
    }
    
    public Object[][] insertMeParameters() throws IllegalArgumentException, IllegalAccessException{
        Class c=this.getClass();
        Object[][] params=new Object[c.getDeclaredFields().length][2];
        int index = 0;
        boolean getid = true;
        for(Field f:c.getDeclaredFields()){
            if(getid){
                getid=false;
            }else{
                if(f.getType().getName().equals("java.lang.String")){
                    params[index][0] = Parametro.STRING;
                    params[index][1] = f.get(this);
                }
                if(f.getType().getName().equals("int")){
                    params[index][0] = Parametro.INT;
                    params[index][1] = f.get(this);
                }
                if(f.getType().getName().equals("double")){
                    params[index][0] = Parametro.DOUBLE;
                    params[index][1] = f.get(this);
                }
                index++;
            }
        }
        return params;
    }
    
    public String updateMe(){
        Class c=this.getClass();
        String tabla=c.getName().substring(c.getName().lastIndexOf(".")+1).toLowerCase();
        String id="";
        String sets="";
        boolean getid=true;
        for(Field f:c.getDeclaredFields()){
            if(getid){
                id=f.getName()+"=?";
                getid=false;
            }else{
                if(sets.equals("")){
                    sets+=f.getName()+"=?";
                }else{
                    sets+=","+f.getName()+"=?";
                }
            }
        }
        return "UPDATE "+tabla+" SET "+sets+" WHERE "+id;
    } 
    
    public Object[][] updateMeParameters() throws IllegalArgumentException, IllegalAccessException{
        Class c=this.getClass();
        Object[][] params=new Object[c.getDeclaredFields().length][2];
        boolean getid=true;
        int index = 0;
        int indexMax = c.getDeclaredFields().length - 1;
        for(Field f:c.getDeclaredFields()){
            if(getid){
                params[indexMax][0] = Parametro.INT;
                params[indexMax][1] = f.get(this);
                getid=false;
            }else{
                if(f.getType().getName().equals("java.lang.String")){
                    params[index][0] = Parametro.STRING;
                    params[index][1] = String.valueOf(f.get(this));
                }
                if(f.getType().getName().equals("int")){
                    params[index][0] = Parametro.INT;
                    params[index][1] = f.get(this);
                }
                if(f.getType().getName().equals("double")){
                    params[index][0] = Parametro.DOUBLE;
                    params[index][1] = f.get(this);
                }
                index++;
            }
        }
        return params;
    }
    
    public String deleteMe(){
        Class c=this.getClass();
        String tabla=c.getName().substring(c.getName().lastIndexOf(".")+1).toLowerCase();
        String id="";
        boolean getid=true;
        for(Field f:c.getDeclaredFields()){
            if(getid){
                id=f.getName()+"=?";
                getid=false;
            }
        }
        return "DELETE FROM "+tabla+" WHERE "+id;
    }
    
    public Object[][] deleteMeParameters() throws IllegalArgumentException, IllegalAccessException{
        Class c=this.getClass();
        Object[][] params=new Object[c.getDeclaredFields().length][2];
        boolean getid=true;
        int index = 0;
        for(Field f:c.getDeclaredFields()){
            if(getid){
                params[index][0] = Parametro.INT;
                params[index][1] = f.get(this);
                getid=false;
            }
        }
        return params;
    }
    
    protected String createMe(){
        Class c=this.getClass();
        String tabla=c.getName().substring(c.getName().lastIndexOf(".")+1).toLowerCase();
        String campos="";
        boolean getid=true;
        for(Field f:c.getDeclaredFields()){
            if(!getid){
                campos+=", ";
            }
            campos+=f.getName();
            if(f.getType().getName().equals("java.lang.String")){
                campos+=" TEXT";
            }
            if(f.getType().getName().equals("int")){
                campos+=" INT";
            }
            if(f.getType().getName().equals("double")){
                campos+=" DOUBLE";
            }    
            if(getid){
                if(f.getType().getName().equals("int")){
                    campos+=" NOT NULL AUTO_INCREMENT, PRIMARY KEY("+f.getName()+")";
                }else{
                    campos+=" NOT NULL, PRIMARY KEY("+f.getName()+")";
                }
                getid=false;
            }else{
                if(f.getName().contains("_")){
                    String table=f.getName().substring(0,f.getName().indexOf("_"));
                    String field=f.getName().substring(f.getName().indexOf("_")+1);
                    campos+=" NOT NULL, KEY "+f.getName()+" ("+f.getName()+"), CONSTRAINT fk_"+tabla+"_"+f.getName()+" FOREIGN KEY ("+f.getName()+") REFERENCES "+table+"("+field+")";
                }else{
                    campos+=" NULL";
                }
            }
        }
        return "CREATE TABLE IF NOT EXISTS "+tabla+" ("+campos+");";
    }   
    
    protected String dropMe(){
        Class c=this.getClass();
        String tabla=c.getName().substring(c.getName().lastIndexOf(".")+1).toLowerCase();
        return "DROP TABLE IF EXISTS "+tabla+";";
    }
    
    public String getFieldString(Field f) throws IllegalArgumentException, IllegalAccessException{
        if(f.getType().getName().equals("java.lang.String")){
            if(f.get(this)!=null){
                return "'"+f.get(this)+"'";
            } 
        }
        if(f.getType().getName().equals("int")){
            if(f.get(this)!=null){
                return String.valueOf(f.get(this));
            }
        }
        if(f.getType().getName().equals("double")){
            if(f.get(this)!=null){
                return String.valueOf(f.get(this));
            }
        }
        return "null";
    }
    
    public Object[] toArrayObjects() throws IllegalArgumentException, IllegalAccessException{
        int index=0;
        Class c=this.getClass();
        Object[] objects=new Object[c.getDeclaredFields().length];
        for(Field f:c.getDeclaredFields()){
            objects[index++]=f.get(this);
        }
        return objects;
    }
    
    public boolean exist(String fieldname){
        Class c=this.getClass();
        for(Field f:c.getDeclaredFields()){
            if(f.getName().equals(fieldname)){
                return true;
            }
        }
        return false;
    }
    
}
