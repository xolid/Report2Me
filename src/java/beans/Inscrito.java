/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Inscrito extends Bean{
    
    protected int idinscrito;
    protected int examen_idexamen;
    protected int escuela_idescuela;
    protected int alumno_idalumno;
    protected int grado_idgrado;
    protected int grupo_idgrupo;

    /**
     * @return the idinscrito
     */
    public int getIdinscrito() {
        return idinscrito;
    }

    /**
     * @param idinscrito the idinscrito to set
     */
    public void setIdinscrito(int idinscrito) {
        this.idinscrito = idinscrito;
    }

    /**
     * @return the examen_idexamen
     */
    public int getExamen_idexamen() {
        return examen_idexamen;
    }

    /**
     * @param examen_idexamen the examen_idexamen to set
     */
    public void setExamen_idexamen(int examen_idexamen) {
        this.examen_idexamen = examen_idexamen;
    }

    /**
     * @return the escuela_idescuela
     */
    public int getEscuela_idescuela() {
        return escuela_idescuela;
    }

    /**
     * @param escuela_idescuela the escuela_idescuela to set
     */
    public void setEscuela_idescuela(int escuela_idescuela) {
        this.escuela_idescuela = escuela_idescuela;
    }

    /**
     * @return the alumno_idalumno
     */
    public int getAlumno_idalumno() {
        return alumno_idalumno;
    }

    /**
     * @param alumno_idalumno the alumno_idalumno to set
     */
    public void setAlumno_idalumno(int alumno_idalumno) {
        this.alumno_idalumno = alumno_idalumno;
    }

    /**
     * @return the grado_idgrado
     */
    public int getGrado_idgrado() {
        return grado_idgrado;
    }

    /**
     * @param grado_idgrado the grado_idgrado to set
     */
    public void setGrado_idgrado(int grado_idgrado) {
        this.grado_idgrado = grado_idgrado;
    }

    /**
     * @return the grupo_idgrupo
     */
    public int getGrupo_idgrupo() {
        return grupo_idgrupo;
    }

    /**
     * @param grupo_idgrupo the grupo_idgrupo to set
     */
    public void setGrupo_idgrupo(int grupo_idgrupo) {
        this.grupo_idgrupo = grupo_idgrupo;
    }

}
