/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Escuela extends Bean{
    
    protected int idescuela;
    protected String nombre;
    protected String direccion;
    protected String telefono;
    protected String centrotrabajo;
    protected String responsable;
    protected int turno;
    protected int supervision_idsupervision;

    /**
     * @return the idescuela
     */
    public int getIdescuela() {
        return idescuela;
    }

    /**
     * @param idescuela the idescuela to set
     */
    public void setIdescuela(int idescuela) {
        this.idescuela = idescuela;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the centrotrabajo
     */
    public String getCentrotrabajo() {
        return centrotrabajo;
    }

    /**
     * @param centrotrabajo the centrotrabajo to set
     */
    public void setCentrotrabajo(String centrotrabajo) {
        this.centrotrabajo = centrotrabajo;
    }

    /**
     * @return the responsable
     */
    public String getResponsable() {
        return responsable;
    }

    /**
     * @param responsable the responsable to set
     */
    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    /**
     * @return the turno
     */
    public int getTurno() {
        return turno;
    }

    /**
     * @param turno the turno to set
     */
    public void setTurno(int turno) {
        this.turno = turno;
    }

    /**
     * @return the supervision_idsupervision
     */
    public int getSupervision_idsupervision() {
        return supervision_idsupervision;
    }

    /**
     * @param supervision_idsupervision the supervision_idsupervision to set
     */
    public void setSupervision_idsupervision(int supervision_idsupervision) {
        this.supervision_idsupervision = supervision_idsupervision;
    }
    
    public static void main(String[] args){
        Escuela escuela = new Escuela();
        System.out.println(escuela.createMe());
    }
    
}
