/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author czarate
 */
public class Subjefatura extends Bean{
    
    protected int idsubjefatura;
    protected String nombre;
    protected String direccion;
    protected String telefono;
    protected String centrotrabajo;
    protected String responsable;

    /**
     * @return the idsubjefatura
     */
    public int getIdsubjefatura() {
        return idsubjefatura;
    }

    /**
     * @param idsubjefatura the idsubjefatura to set
     */
    public void setIdsubjefatura(int idsubjefatura) {
        this.idsubjefatura = idsubjefatura;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the centrotrabajo
     */
    public String getCentrotrabajo() {
        return centrotrabajo;
    }

    /**
     * @param centrotrabajo the centrotrabajo to set
     */
    public void setCentrotrabajo(String centrotrabajo) {
        this.centrotrabajo = centrotrabajo;
    }

    /**
     * @return the responsable
     */
    public String getResponsable() {
        return responsable;
    }

    /**
     * @param responsable the responsable to set
     */
    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }
    
    public static void main(String[] args){
        Subjefatura subjefatura = new Subjefatura();
        System.out.println(subjefatura.createMe());
    }

}