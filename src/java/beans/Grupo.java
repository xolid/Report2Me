/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Grupo extends Bean{
    
    protected int idgrupo;
    protected String nombre;

    /**
     * @return the idgrupo
     */
    public int getIdgrupo() {
        return idgrupo;
    }

    /**
     * @param idgrupo the idgrupo to set
     */
    public void setIdgrupo(int idgrupo) {
        this.idgrupo = idgrupo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public static void main(String[] args){
        Grupo grupo = new Grupo();
        System.out.println(grupo.createMe());
    }
    
}
