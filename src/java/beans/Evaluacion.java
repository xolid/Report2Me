/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Evaluacion extends Bean{
    
    protected int idevaluacion;
    protected int grado_idgrado;
    protected int examen_idexamen;
    protected int materia_idmateria;

    /**
     * @return the idevaluacion
     */
    public int getIdevaluacion() {
        return idevaluacion;
    }

    /**
     * @param idevaluacion the idevaluacion to set
     */
    public void setIdevaluacion(int idevaluacion) {
        this.idevaluacion = idevaluacion;
    }

    /**
     * @return the grado_idgrado
     */
    public int getGrado_idgrado() {
        return grado_idgrado;
    }

    /**
     * @param grado_idgrado the grado_idgrado to set
     */
    public void setGrado_idgrado(int grado_idgrado) {
        this.grado_idgrado = grado_idgrado;
    }

    /**
     * @return the examen_idexamen
     */
    public int getExamen_idexamen() {
        return examen_idexamen;
    }

    /**
     * @param examen_idexamen the examen_idexamen to set
     */
    public void setExamen_idexamen(int examen_idexamen) {
        this.examen_idexamen = examen_idexamen;
    }

    /**
     * @return the materia_idmateria
     */
    public int getMateria_idmateria() {
        return materia_idmateria;
    }

    /**
     * @param materia_idmateria the materia_idmateria to set
     */
    public void setMateria_idmateria(int materia_idmateria) {
        this.materia_idmateria = materia_idmateria;
    }
    
}
