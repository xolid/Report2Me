/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.Arrays;

/**
 *
 * @author czarate
 */
public class Usuario extends Bean{
    
    protected int idusuario;
    protected String nombre;
    protected String mail;
    protected String pass;
    protected int nivel;
    protected String fingreso;
    protected String fegreso;

    /**
     * @return the idusuario
     */
    public int getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return the nivel
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * @param nivel the nivel to set
     */
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    /**
     * @return the fingreso
     */
    public String getFingreso() {
        return fingreso;
    }

    /**
     * @param fingreso the fingreso to set
     */
    public void setFingreso(String fingreso) {
        this.fingreso = fingreso;
    }

    /**
     * @return the fegreso
     */
    public String getFegreso() {
        return fegreso;
    }

    /**
     * @param fegreso the fegreso to set
     */
    public void setFegreso(String fegreso) {
        this.fegreso = fegreso;
    }
    
    public static void main(String[] args){
        Usuario usuario = new Usuario();
        System.out.println(usuario.createMe());
    }

}
