/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Grado extends Bean{
    
    protected int idgrado;
    protected String nombre;

    /**
     * @return the idgrado
     */
    public int getIdgrado() {
        return idgrado;
    }

    /**
     * @param idgrado the idgrado to set
     */
    public void setIdgrado(int idgrado) {
        this.idgrado = idgrado;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public static void main(String[] args){
        Grado grado = new Grado();
        System.out.println(grado.createMe());
    }
    
}
