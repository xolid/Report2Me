/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Estatus extends Bean{
    
    protected int idestatus;
    protected int inscripcion;
    protected int calificacion;
    protected int examen_idexamen;
    protected int escuela_idescuela;

    /**
     * @return the idestatus
     */
    public int getIdestatus() {
        return idestatus;
    }

    /**
     * @param idestatus the idestatus to set
     */
    public void setIdestatus(int idestatus) {
        this.idestatus = idestatus;
    }

    /**
     * @return the inscripcion
     */
    public int getInscripcion() {
        return inscripcion;
    }

    /**
     * @param inscripcion the inscripcion to set
     */
    public void setInscripcion(int inscripcion) {
        this.inscripcion = inscripcion;
    }

    /**
     * @return the calificacion
     */
    public int getCalificacion() {
        return calificacion;
    }

    /**
     * @param calificacion the calificacion to set
     */
    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    /**
     * @return the examen_idexamen
     */
    public int getExamen_idexamen() {
        return examen_idexamen;
    }

    /**
     * @param examen_idexamen the examen_idexamen to set
     */
    public void setExamen_idexamen(int examen_idexamen) {
        this.examen_idexamen = examen_idexamen;
    }

    /**
     * @return the escuela_idescuela
     */
    public int getEscuela_idescuela() {
        return escuela_idescuela;
    }

    /**
     * @param escuela_idescuela the escuela_idescuela to set
     */
    public void setEscuela_idescuela(int escuela_idescuela) {
        this.escuela_idescuela = escuela_idescuela;
    }
    
}
