/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Respuesta extends Bean{
 
    protected int idrespuesta;
    protected int opcion;
    protected int pregunta_idpregunta;
    protected int inscrito_idinscrito;

    /**
     * @return the idrespuesta
     */
    public int getIdrespuesta() {
        return idrespuesta;
    }

    /**
     * @param idrespuesta the idrespuesta to set
     */
    public void setIdrespuesta(int idrespuesta) {
        this.idrespuesta = idrespuesta;
    }

    /**
     * @return the opcion
     */
    public int getOpcion() {
        return opcion;
    }

    /**
     * @param opcion the opcion to set
     */
    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }

    /**
     * @return the pregunta_idpregunta
     */
    public int getPregunta_idpregunta() {
        return pregunta_idpregunta;
    }

    /**
     * @param pregunta_idpregunta the pregunta_idpregunta to set
     */
    public void setPregunta_idpregunta(int pregunta_idpregunta) {
        this.pregunta_idpregunta = pregunta_idpregunta;
    }

    /**
     * @return the inscrito_idinscrito
     */
    public int getInscrito_idinscrito() {
        return inscrito_idinscrito;
    }

    /**
     * @param inscrito_idinscrito the inscrito_idinscrito to set
     */
    public void setInscrito_idinscrito(int inscrito_idinscrito) {
        this.inscrito_idinscrito = inscrito_idinscrito;
    }
    
}
