/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Pregunta extends Bean{
    
    protected int idpregunta;
    protected int indice;
    protected int opciones;
    protected int correcta;
    protected int evaluacion_idevaluacion;

    /**
     * @return the idpregunta
     */
    public int getIdpregunta() {
        return idpregunta;
    }

    /**
     * @param idpregunta the idpregunta to set
     */
    public void setIdpregunta(int idpregunta) {
        this.idpregunta = idpregunta;
    }

    /**
     * @return the indice
     */
    public int getIndice() {
        return indice;
    }

    /**
     * @param indice the indice to set
     */
    public void setIndice(int indice) {
        this.indice = indice;
    }

    /**
     * @return the opciones
     */
    public int getOpciones() {
        return opciones;
    }

    /**
     * @param opciones the opciones to set
     */
    public void setOpciones(int opciones) {
        this.opciones = opciones;
    }

    /**
     * @return the correcta
     */
    public int getCorrecta() {
        return correcta;
    }

    /**
     * @param correcta the correcta to set
     */
    public void setCorrecta(int correcta) {
        this.correcta = correcta;
    }

    /**
     * @return the evaluacion_idevaluacion
     */
    public int getEvaluacion_idevaluacion() {
        return evaluacion_idevaluacion;
    }

    /**
     * @param evaluacion_idevaluacion the evaluacion_idevaluacion to set
     */
    public void setEvaluacion_idevaluacion(int evaluacion_idevaluacion) {
        this.evaluacion_idevaluacion = evaluacion_idevaluacion;
    }
    
}
