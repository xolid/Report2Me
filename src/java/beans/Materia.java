/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Materia extends Bean {
    
    protected int idmateria;
    protected String nombre;

    /**
     * @return the idmateria
     */
    public int getIdmateria() {
        return idmateria;
    }

    /**
     * @param idmateria the idmateria to set
     */
    public void setIdmateria(int idmateria) {
        this.idmateria = idmateria;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
