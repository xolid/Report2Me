/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Examen extends Bean{
    
    protected int idexamen;
    protected String nombre;
    protected String fecha;

    /**
     * @return the idexamen
     */
    public int getIdexamen() {
        return idexamen;
    }

    /**
     * @param idexamen the idexamen to set
     */
    public void setIdexamen(int idexamen) {
        this.idexamen = idexamen;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
}
