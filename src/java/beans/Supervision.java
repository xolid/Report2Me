/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author solid
 */
public class Supervision extends Bean{
    
    protected int idsupervision;
    protected String nombre;
    protected String direccion;
    protected String telefono;
    protected String centrotrabajo;
    protected String zonaescolar;
    protected String responsable;
    protected int subjefatura_idsubjefatura;

    /**
     * @return the idsupervision
     */
    public int getIdsupervision() {
        return idsupervision;
    }

    /**
     * @param idsupervision the idsupervision to set
     */
    public void setIdsupervision(int idsupervision) {
        this.idsupervision = idsupervision;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the centrotrabajo
     */
    public String getCentrotrabajo() {
        return centrotrabajo;
    }

    /**
     * @param centrotrabajo the centrotrabajo to set
     */
    public void setCentrotrabajo(String centrotrabajo) {
        this.centrotrabajo = centrotrabajo;
    }

    /**
     * @return the zonaescolar
     */
    public String getZonaescolar() {
        return zonaescolar;
    }

    /**
     * @param zonaescolar the zonaescolar to set
     */
    public void setZonaescolar(String zonaescolar) {
        this.zonaescolar = zonaescolar;
    }

    /**
     * @return the responsable
     */
    public String getResponsable() {
        return responsable;
    }

    /**
     * @param responsable the responsable to set
     */
    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    /**
     * @return the subjefatura_idsubjefatura
     */
    public int getSubjefatura_idsubjefatura() {
        return subjefatura_idsubjefatura;
    }

    /**
     * @param subjefatura_idsubjefatura the subjefatura_idsubjefatura to set
     */
    public void setSubjefatura_idsubjefatura(int subjefatura_idsubjefatura) {
        this.subjefatura_idsubjefatura = subjefatura_idsubjefatura;
    }
    
    public static void main(String[] args){
        Supervision supervision = new Supervision();
        System.out.println(supervision.createMe());
    }
    
}
