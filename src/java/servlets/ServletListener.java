/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package servlets;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Web application lifecycle listener.
 * @author solid
 */

public class ServletListener implements HttpSessionListener {

    private static int inActivas = 0;
    private static java.util.List<javax.servlet.http.HttpSession> sessions = new java.util.ArrayList();

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        sessions.add(se.getSession());
        inActivas++;
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        sessions.remove(se.getSession());
        if(inActivas>0)
            inActivas--;
    }

    public static int getActivas() {
        return inActivas;
    }

    public static java.util.List<javax.servlet.http.HttpSession> getSessions(){
        return sessions;
    }

}