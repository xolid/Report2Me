<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<%@page import="java.io.File"%>
<%@page import="beans.Usuario"%>
<%@page import="auxiliares.Nivel"%>
<jsp:useBean id="usuario" class="beans.Usuario" scope="page"/>
<jsp:setProperty name="usuario" property="*" />

<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarUsuario(usuario);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarUsuario(usuario);        
    }
}
if(request.getParameter("id")!=null){
    int idusuario=Integer.parseInt((String)request.getParameter("id"));
    usuario=facade.getById(idusuario);
    usuario.setFegreso(usuario.getFegreso()==null?"":usuario.getFegreso());
    usuario.setPass("");
}else{
    usuario.setIdusuario(0);
    usuario.setNombre("");
    usuario.setMail("");
    usuario.setPass("");
    usuario.setNivel(0);
    usuario.setFingreso(new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date()));
    usuario.setFegreso("");
}
int index=0;
if(request.getParameter("index")!=null){
    index=Integer.parseInt(request.getParameter("index"));
}
String campo="nombre";
if(request.getParameter("campo")!=null){
    campo=request.getParameter("campo");
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Usuario</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos del Usuario
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="usuario.jsp" method="post">
                        <input type="hidden" id="idusuario" name="idusuario" value="<%=usuario.getIdusuario()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" value="<%=usuario.getNombre()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="mail">Mail</label>
                                <input class="form-control" id="mail" name="mail" type="email" placeholder="Mail" maxlength="75" value="<%=usuario.getMail()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input class="form-control" id="pass" name="pass" type="password" placeholder="Password" value="<%=usuario.getPass()%>" required>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:actualizarMD5();">Guardar</button>
                            <%if(usuario.getIdusuario()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Usuario/usuario.jsp','Usuario/divlistado.jsp?index=<%=index%>&campo=<%=campo%>','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Usuario/divlistado.jsp?index=<%=index%>&campo=<%=campo%>','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Fecha de Ingreso</label>
                                <input class="form-control" id="fingreso" name="fingreso" type="date" value="<%=usuario.getFingreso()%>" required>
                            </div>
                            <div class="form-group">
                                <label>Fecha de Egreso</label>
                                <input class="form-control" id="fegreso" name="fegreso" type="date" value="<%=usuario.getFegreso()%>">
                            </div>
                            <div class="form-group">
                                <label for="nivel">Nivel de Privilegios</label>
                                <select id="nivel" name="nivel" class="form-control">
                                    <%for(Nivel nivel:new auxiliares.Niveles().getNiveles()){
                                        beans.Usuario u = (beans.Usuario)session.getAttribute("usuario");
                                        if(nivel.getIndice()<5){%>
                                            <option value="<%=nivel.getIndice()%>" <%=nivel.getIndice()==usuario.getNivel()?"SELECTED":""%>><%=nivel.getNombre()%></option>
                                        <%}else{
                                            if(u.getNivel()==5){%>
                                                <option value="<%=nivel.getIndice()%>" <%=nivel.getIndice()==usuario.getNivel()?"SELECTED":""%>><%=nivel.getNombre()%></option>
                                            <%}
                                        }%>
                                    <%}%>
                                </select>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->
<script lang="javascript">
    function actualizarMD5(){
        if(document.form1.pass.value!==""){
            document.form1.pass.value=hex_md5(document.form1.pass.value);
            save('Usuario/usuario.jsp','Usuario/divlistado.jsp?index=<%=index%>&campo=<%=campo%>','form1','page-wrapper');
        }else{
            alert('Actualizar el usuario, requiere reinicar su password...');
        }
    }
</script>

<%facade.close();%>