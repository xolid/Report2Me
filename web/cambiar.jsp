<%-- 
    Document   : cambiar
    Created on : Sep 24, 2014, 9:00:20 PM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="usuario" class="beans.Usuario" scope="page"/>
<jsp:setProperty name="usuario" property="*" />

<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>

<%
if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        if(request.getParameter("pass0")!=null&&request.getParameter("pass1")!=null&&request.getParameter("pass2")!=null){
            String pass0=request.getParameter("pass0");
            String pass1=request.getParameter("pass1");
            String pass2=request.getParameter("pass2");
            if(!pass0.equals("null")&&!pass1.equals("null")&&!pass2.equals("null")){
                if(usuario.getPass().equals(pass0)){
                    if(pass1.equals(pass2)){
                        beans.Usuario u = facade.getById(usuario.getIdusuario());
                        u.setPass(pass2);
                        facade.salvarUsuario(u);
                    }
                }
            }
        }
    }
}
if(session.getAttribute("usuario")!=null){
    beans.Usuario u = (beans.Usuario)session.getAttribute("usuario");
    usuario = facade.getById(u.getIdusuario());
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Cambiar Password</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos del Usuario
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form role="form"id="form1" name="form1" action="cambiar.jsp" method="post">
                            <input type="hidden" id="idusuario" name="idusuario" value="<%=usuario.getIdusuario()%>"/>
                            <input type="hidden" id="pass" name="pass" value="<%=usuario.getPass()%>"/>
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" value="<%=usuario.getNombre()%>" disabled>
                            </div>
                            <div class="form-group">
                                <label for="mail">Mail</label>
                                <input class="form-control" id="mail" name="mail" type="mail" placeholder="Mail" value="<%=usuario.getMail()%>" disabled>
                            </div>
                            <div class="form-group">
                                <label>Password Actual</label>
                                <input class="form-control" id="pass0" name="pass0" type="password" placeholder="Actual" required>
                            </div>
                            <div class="form-group">
                                <label>Password Nuevo</label>
                                <input class="form-control" id="pass1" name="pass1" type="password" placeholder="Nuevo" required>
                            </div>
                            <div class="form-group">
                                <label>Reescribir Password</label>
                                <input class="form-control" id="pass2" name="pass2" type="password" placeholder="Reescribir" required>
                            </div>
                            <button class="btn btn-danger" type="button" onclick="javascript:actualizarMD5();">Guardar</button>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
    function actualizarMD5(){
        document.form1.pass0.value=hex_md5(document.form1.pass0.value);
        document.form1.pass1.value=hex_md5(document.form1.pass1.value);
        document.form1.pass2.value=hex_md5(document.form1.pass2.value);
        save('cambiar.jsp','cambiar.jsp','form1','page-wrapper');
    }
</script>

<%
facade.close();
%>