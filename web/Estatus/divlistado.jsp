<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="facade" scope="page" class="facades.EstatusFacade"/>
<jsp:useBean id="efacade" scope="page" class="facades.EscuelaFacade"/>
<jsp:useBean id="exfacade" scope="page" class="facades.ExamenFacade"/>

<%
int index=0;
if(request.getParameter("index")!=null){
    index=Integer.parseInt(request.getParameter("index"));
}
int idexamen=0;
if(request.getParameter("idexamen")!=null){
    idexamen=Integer.parseInt(request.getParameter("idexamen"));
}
int size=10;
long total=(Long)facade.getAllTotal();
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Estatus</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="form-group">
                    <label for="examen_idexamen">Examen:</label>
                    <select class="form-control" id="examen_idexamen" onchange="javascript:go2to('Estatus/divlistado.jsp?index=<%=index%>&idexamen='+document.getElementById('examen_idexamen').options[document.getElementById('examen_idexamen').selectedIndex].value,'page-wrapper');">
                        <%for(beans.Examen examen:exfacade.getAll()){%>
                            <option value="<%=examen.getIdexamen()%>" <%=(examen.getIdexamen()==idexamen?"SELECTED":"")%>><%=examen.getNombre()%></option>
                        <%}%>
                    </select>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-9">Escuela</th>
                                <th class="center-col-lg-1">Inscripcion</th>
                                <th class="center-col-lg-1">Calificacion</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(beans.Estatus e:facade.getByExamenLimited(idexamen, index, size)){%>
                                <tr>
                                    <td><%=efacade.getById(e.getEscuela_idescuela()).getNombre()%></td>
                                    <td class="center-col-lg-1"><%=(e.getInscripcion()==0?"NO":"SI")%></td>
                                    <td class="center-col-lg-1"><%=(e.getCalificacion()==0?"NO":"SI")%></td>
                                    <td class="center-col-lg-1">
                                        <button type="button" class="btn btn-warning btn-xs" onclick="javascript:go2to('Estatus/estatus.jsp?id=<%=e.getIdestatus()%>&index=<%=index%>&idexamen='+document.getElementById('examen_idexamen').options[document.getElementById('examen_idexamen').selectedIndex].value,'page-wrapper')"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <%}%>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                    <div class="col-lg-12 col-center">
                        <ul class="pagination">
                            <%if(index+size<=total){%>
                                <li class="next"><a href="#" onclick="javascript:go2to('Estatus/divlistado.jsp?index=<%=index+size%>&idexamen='+document.getElementById('examen_idexamen').options[document.getElementById('examen_idexamen').selectedIndex].value,'page-wrapper');">>></a></li>
                            <%}else{%>
                                <li class="next disabled"><a>>></a></li>
                            <%}%>
                            <li><a>Pagina <%=(index/size+1)+" de "+(total/size+1)%></a></li>
                            <%if(index-size>=0){%>
                                <li class="prev"><a href="#" onclick="javascript:go2to('Estatus/divlistado.jsp?index=<%=index-size%>&idexamen='+document.getElementById('examen_idexamen').options[document.getElementById('examen_idexamen').selectedIndex].value,'page-wrapper');"><<</a></li>
                            <%}else{%>
                                <li class="prev disabled"><a href="#"><<</a></li>
                            <%}%>
                        </ul>
                    </div>
                    <!-- /. div-pagination -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<script>
    var idexamen = <%=idexamen%>;
    if(idexamen===0){
        go2to('Estatus/divlistado.jsp?index=<%=index%>&idexamen='+document.getElementById('examen_idexamen').options[document.getElementById('examen_idexamen').selectedIndex].value,'page-wrapper');
    }
</script>                            
                            
<%
facade.close();
%>