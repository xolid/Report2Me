<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<jsp:useBean id="estatus" class="beans.Estatus" scope="page"/>
<jsp:setProperty name="estatus" property="*" />

<jsp:useBean id="facade" scope="page" class="facades.EstatusFacade"/>
<jsp:useBean id="efacade" scope="page" class="facades.EscuelaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarEstatus(estatus);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarEstatus(estatus);        
    }
}
if(request.getParameter("id")!=null){
    int idestatus=Integer.parseInt((String)request.getParameter("id"));
    estatus=facade.getById(idestatus);
}
int index=0;
if(request.getParameter("index")!=null){
    index=Integer.parseInt(request.getParameter("index"));
}
int idexamen=0;
if(request.getParameter("idexamen")!=null){
    idexamen=Integer.parseInt(request.getParameter("idexamen"));
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Estatus</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos del Estatus de la Escuela: [<%=efacade.getById(estatus.getEscuela_idescuela()).getNombre()%>]
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="estatus.jsp" method="post">
                        <input type="hidden" id="idestatus" name="idestatus" value="<%=estatus.getIdestatus()%>"/>
                        <input type="hidden" id="examen_idexamen" name="examen_idexamen" value="<%=estatus.getExamen_idexamen()%>"/>
                        <input type="hidden" id="escuela_idescuela" name="escuela_idescuela" value="<%=estatus.getEscuela_idescuela()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="inscripcion">Terminada la Inscripcion?</label>
                                <select id="inscripcion" name="inscripcion" class="form-control">
                                    <option value="0" <%=estatus.getInscripcion()==0?"SELECTED":""%>>NO</option>
                                    <option value="1" <%=estatus.getInscripcion()==1?"SELECTED":""%>>SI</option>
                                </select>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Estatus/estatus.jsp','Estatus/divlistado.jsp?index=<%=index%>&idexamen=<%=idexamen%>','form1','page-wrapper');">Guardar</button>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Estatus/divlistado.jsp?index=<%=index%>&idexamen=<%=idexamen%>','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="calificacion">Terminada la Calificacion?</label>
                                <select id="calificacion" name="calificacion" class="form-control">
                                    <option value="0" <%=estatus.getCalificacion()==0?"SELECTED":""%>>NO</option>
                                    <option value="1" <%=estatus.getCalificacion()==1?"SELECTED":""%>>SI</option>
                                </select>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
efacade.close();
%>