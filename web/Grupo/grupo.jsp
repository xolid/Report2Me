<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<jsp:useBean id="grupo" class="beans.Grupo" scope="page"/>
<jsp:setProperty name="grupo" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.GrupoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarGrupo(grupo);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarGrupo(grupo);        
    }
}
if(request.getParameter("id")!=null){
    int idgrupo=Integer.parseInt((String)request.getParameter("id"));
    grupo=facade.getById(idgrupo);
}else{
    grupo.setIdgrupo(0);
    grupo.setNombre("");
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Grupo</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos del Grupo
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="grupo.jsp" method="post">
                        <input type="hidden" id="idgrupo" name="idgrupo" value="<%=grupo.getIdgrupo()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" value="<%=grupo.getNombre()%>" required>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Grupo/grupo.jsp','Grupo/divlistado.jsp','form1','page-wrapper');">Guardar</button>
                            <%if(grupo.getIdgrupo()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Grupo/grupo.jsp','Grupo/divlistado.jsp','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Grupo/divlistado.jsp','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6"></div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>