<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<jsp:useBean id="grado" class="beans.Grado" scope="page"/>
<jsp:setProperty name="grado" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.GradoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarGrado(grado);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarGrado(grado);        
    }
}
if(request.getParameter("id")!=null){
    int idgrado=Integer.parseInt((String)request.getParameter("id"));
    grado=facade.getById(idgrado);
}else{
    grado.setIdgrado(0);
    grado.setNombre("");
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Grado</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos del Grado
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="grado.jsp" method="post">
                        <input type="hidden" id="idgrado" name="idgrado" value="<%=grado.getIdgrado()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" value="<%=grado.getNombre()%>" required>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Grado/grado.jsp','Grado/divlistado.jsp','form1','page-wrapper');">Guardar</button>
                            <%if(grado.getIdgrado()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Grado/grado.jsp','Grado/divlistado.jsp','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Grado/divlistado.jsp','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6"></div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>