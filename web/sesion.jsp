<%-- 
    Document   : error
    Created on : Oct 10, 2014, 11:26:57 PM
    Author     : CarlosAlberto
--%>

<div class="row">
    <div class="col-lg-12">&nbsp;</div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-warning">
            <p><strong>Su sesion ha terminado...</strong></p>
            <br>
            <p>El sistema solo permite mantener una sesion inactiva por 15 minutos, por favor vuelva a iniciar sesion.</p>
        </div>
    </div>
    <!-- /.col-lg-6 -->
</div>