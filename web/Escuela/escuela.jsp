<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<%@page import="java.io.File"%>
<%@page import="beans.Escuela"%>
<%@page import="auxiliares.Nivel"%>
<jsp:useBean id="escuela" class="beans.Escuela" scope="page"/>
<jsp:setProperty name="escuela" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.EscuelaFacade"/>

<jsp:useBean id="supervision" class="beans.Supervision" scope="page"/>
<jsp:useBean id="sfacade" scope="page" class="facades.SupervisionFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarEscuela(escuela);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarEscuela(escuela);        
    }
}
if(request.getParameter("id")!=null){
    int idescuela=Integer.parseInt((String)request.getParameter("id"));
    escuela=facade.getById(idescuela);
    supervision = sfacade.getById(escuela.getSupervision_idsupervision());
}else{
    escuela.setIdescuela(0);
    escuela.setNombre("");
    escuela.setDireccion("");
    escuela.setTelefono("");
    escuela.setCentrotrabajo("");
    escuela.setResponsable("");
    escuela.setTurno(0);
    if(request.getParameter("supervision")!=null){
        int idsupervision = Integer.parseInt(request.getParameter("supervision"));
        supervision = sfacade.getById(idsupervision);
    }
    escuela.setSupervision_idsupervision(supervision.getIdsupervision());
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Escuela</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos de la Escuela en la Supervision [<%=supervision.getNombre()%>]
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="escuela.jsp" method="post">
                        <input type="hidden" id="idescuela" name="idescuela" value="<%=escuela.getIdescuela()%>"/>
                        <input type="hidden" id="supervision_idsupervision" name="supervision_idsupervision" value="<%=escuela.getSupervision_idsupervision()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" maxlength="50" value="<%=escuela.getNombre()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="direccion">Direccion</label>
                                <input class="form-control" id="direccion" name="direccion" type="text" placeholder="Direccion" value="<%=escuela.getDireccion()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="telefono">Telefono</label>
                                <input class="form-control" id="telefono" name="telefono" type="text" placeholder="Telefono" value="<%=escuela.getTelefono()%>" required>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Escuela/escuela.jsp','Escuela/divlistado.jsp?supervision=<%=supervision.getIdsupervision()%>','form1','page-wrapper');">Guardar</button>
                            <%if(escuela.getIdescuela()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Escuela/escuela.jsp','Escuela/divlistado.jsp?supervision=<%=supervision.getIdsupervision()%>','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Escuela/divlistado.jsp?supervision=<%=supervision.getIdsupervision()%>','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="centrotrabajo">Centro de Trabajo</label>
                                <input class="form-control" id="centrotrabajo" name="centrotrabajo" type="text" placeholder="Centro de Trabajo" value="<%=escuela.getCentrotrabajo()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="responsable">Responsable</label>
                                <input class="form-control" id="responsable" name="responsable" type="text" placeholder="Responsable" value="<%=escuela.getResponsable()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="turno">Turno</label>
                                <select class="form-control" id="turno" name="turno">
                                    <%for(auxiliares.Turno turno:new auxiliares.Turnos().getTurnos()){%>
                                        <option value="<%=turno.getIndice()%>" <%=turno.getIndice()==escuela.getTurno()?"SELECTED":""%>><%=turno.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%facade.close();%>