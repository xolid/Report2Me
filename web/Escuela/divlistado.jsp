<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="supervision" class="beans.Supervision" scope="page"/>

<jsp:useBean id="ssfacade" scope="page" class="facades.SubjefaturaFacade"/>
<jsp:useBean id="sfacade" scope="page" class="facades.SupervisionFacade"/>
<jsp:useBean id="facade" scope="page" class="facades.EscuelaFacade"/>

<%
java.util.List<auxiliares.Navegacion> navegaciones = new java.util.ArrayList();
if(request.getParameter("supervision")!=null){
    int idsupervision=Integer.parseInt(request.getParameter("supervision"));
    supervision = sfacade.getById(idsupervision);
    
    if(session.getAttribute("navegaciones")!=null){
        navegaciones = (java.util.List<auxiliares.Navegacion>)session.getAttribute("navegaciones");
    }
    auxiliares.Navegacion navegacion = new auxiliares.Navegacion();
    navegacion.setUrl("Escuela/divlistado.jsp?supervision="+supervision.getIdsupervision());
    navegacion.setNombre(supervision.getNombre());
    if(navegaciones.size()>1){
        navegaciones=navegaciones.subList(0,2);
        navegaciones.set(1,navegacion);
    }else{
        navegaciones.add(navegacion);
    }
    session.setAttribute("navegaciones",navegaciones);
    
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Escuela</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Listado de Escuelas Registradas de la Supervision [<%=supervision.getNombre()%>]
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-5">Nombre</th>
                                <th class="col-lg-6">Direccion</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(beans.Escuela s:facade.getBySupervision(supervision.getIdsupervision())){%>
                                <tr>
                                    <td><%=s.getNombre()%></td>
                                    <td><%=s.getDireccion()%></td>
                                    <td class="center-col-lg-1">
                                        <button type="button" class="btn btn-warning btn-xs" onclick="javascript:go2to('Escuela/escuela.jsp?id=<%=s.getIdescuela()%>','page-wrapper')"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <%}%>
                            <tr>
                                <td colspan="2"></td>
                                <td class="center-col-lg-1">
                                    <button type="button" class="btn btn-success btn-xs" onclick="javascript:go2to('Escuela/escuela.jsp?supervision=<%=supervision.getIdsupervision()%>','page-wrapper');"><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                    <div class="col-lg-12 col-center">
                        <ul class="pagination">
                            <%java.util.Collections.reverse(navegaciones);
                            for(auxiliares.Navegacion navegacion:navegaciones){%>
                                <li><a href="#" onclick="javascript:go2to('<%=navegacion.getUrl()%>','page-wrapper');"><%=navegacion.getNombre()%> ></a></li>
                            <%}
                            java.util.Collections.reverse(navegaciones);%>
                            <li><a href="#" onclick="javascript:go2to('Subjefatura/divlistado.jsp','page-wrapper');">SUBJEFATURAS ></a></li>
                        </ul>
                    </div>
                    <!-- /. div-pagination -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<%
facade.close();
sfacade.close();
ssfacade.close();
%>