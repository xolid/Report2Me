<%-- 
    Document   : principal
    Created on : Sep 24, 2014, 2:08:39 PM
    Author     : CarlosAlberto
--%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="sistema de consulta de resultados">
        <meta name="author" content="Ing. Carlos Zarate">
        <title>Reporter - Consulta de Resultados</title>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico">
        <!-- Bootstrap Core CSS -->
        <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="<%=request.getContextPath()%>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<%=request.getContextPath()%>/css/sb-admin-2.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="<%=request.getContextPath()%>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/Chart.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/ajax.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/control.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/interpretador.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Mostrar Navegacion</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="principal.jsp">Reporter - Consulta de Resultados</a>
                </div>
                <!-- /.navbar-header -->
                <ul class="nav navbar-top-links navbar-right">
                    <%if(session.getAttribute("usuario")!=null){
                        beans.Usuario u = (beans.Usuario)session.getAttribute("usuario");
                        if(u.getNivel()>2){%>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-gear fa-fw"></i>
                                    <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu">    
                                    <li><a href="#" onclick="javascript:go2to('Conexion/divlistado.jsp','page-wrapper');"><i class="fa fa-users fa-fw"></i> Conexiones al Sistema</a></li>
                                    <li><a href="#" onclick="javascript:go2to('Alumno/divlistado2.jsp','page-wrapper');"><i class="fa fa-warning fa-fw"></i> Alumnos Pendientes</a></li>
                                </ul>
                                <!-- /.dropdown-user -->
                            </li>
                        <%}
                    }%>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdow-user">    
                            <%if(session.getAttribute("usuario")!=null){
                                beans.Usuario u = (beans.Usuario)session.getAttribute("usuario");
                                out.print("<li><a><i class=\"fa fa-user fa-fw\"></i> "+u.getNombre()+"</a></li>");
                                out.print("<li><a href=\"#\" onclick=\"javascript:go2to('cambiar.jsp','page-wrapper');\"><i class=\"fa fa-key fa-fw\"></i> Cambiar Password</a></li>");
                            }else{
                                out.print("<li><a><i class=\"fa fa-user fa-fw\"></i>Sesion Terminada</a></li>");
                            }%>
                            <li class="divider"></li>
                            <li><a href="index.jsp"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesion</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <%if(session.getAttribute("usuario")!=null){
                                beans.Usuario u = (beans.Usuario)session.getAttribute("usuario");
                                if(u.getNivel()>0){%>
                                    <li>
                                        <a href="#" onclick="javascript:go2to('dashboard.jsp','page-wrapper');"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="javascript:go2to('Calificar/divlistado.jsp','page-wrapper');"><i class="fa fa-check-square fa-fw"></i> Calificar</a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="javascript:go2to('Inscrito/divlistado.jsp','page-wrapper');"><i class="fa fa-list fa-fw"></i> Inscribir</a>
                                    </li>
                                <%}if(u.getNivel()>1){%>    
                                    <li>
                                        <a href="#"><i class="fa fa-building fa-fw"></i> Admon. de Organigrama<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li>
                                                <a href="#" onclick="javascript:go2to('Alumno/divlistado.jsp','page-wrapper');">Alumnos</a>
                                            </li>  
                                            <%if(u.getNivel()>2){%>
                                                <li>
                                                    <a href="#" onclick="javascript:go2to('Grado/divlistado.jsp','page-wrapper');">Grados</a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="javascript:go2to('Grupo/divlistado.jsp','page-wrapper');">Grupos</a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="javascript:go2to('Subjefatura/divlistado.jsp','page-wrapper');">Organigrama</a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="javascript:go2to('Asignacion/divlistado.jsp','page-wrapper');">Asignaciones</a>
                                                </li>
                                            <%}%>
                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>
                                <%}if(u.getNivel()>2){%>
                                    <li>
                                        <a href="#"><i class="fa fa-desktop fa-fw"></i> Dise�o de Examenes<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li>
                                                <a href="#" onclick="javascript:go2to('Examen/divlistado.jsp','page-wrapper');">Examen</a>
                                            </li>
                                            <li>
                                                <a href="#" onclick="javascript:go2to('Estatus/divlistado.jsp','page-wrapper');">Estatus</a>
                                            </li>
                                            <li>
                                                <a href="#" onclick="javascript:go2to('Materia/divlistado.jsp','page-wrapper');">Materias</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#" onclick="javascript:go2to('Reporte/divlistado.jsp','page-wrapper');"><i class="fa fa-info-circle fa-fw"></i> Reportes del Sistema</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Catalogos del Sistema<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li>
                                                <a href="#" onclick="javascript:go2to('Usuario/divlistado.jsp','page-wrapper');">Usuarios</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>
                                <%}
                            }%>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>
            <div id="page-wrapper"></div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- javascript MD5 -->
        <script src="<%=request.getContextPath()%>/js/md5.js"></script>
        <!-- jQuery Version 1.11.0 -->
        <script src="<%=request.getContextPath()%>/js/jquery-1.11.0.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
        <!-- Metis Menu Plugin JavaScript -->
        <script src="<%=request.getContextPath()%>/js/plugins/metisMenu/metisMenu.min.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="<%=request.getContextPath()%>/js/sb-admin-2.js"></script>
        <script lang="javascript">
            go2to('dashboard.jsp','page-wrapper');
        </script>
    </body>
</html>