<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="examenfacade" scope="page" class="facades.ExamenFacade"/>
<jsp:useBean id="escuelafacade" scope="page" class="facades.EscuelaFacade"/>
<jsp:useBean id="gradofacade" scope="page" class="facades.GradoFacade"/>
<jsp:useBean id="grupofacade" scope="page" class="facades.GrupoFacade"/>

<script>
    function update(){
        close('calificar');
        try{
            var idexamen = document.getElementById('examen_idexamen').options[document.getElementById('examen_idexamen').selectedIndex].value;
            var idescuela = document.getElementById('escuela_idescuela').options[document.getElementById('escuela_idescuela').selectedIndex].value;
            var idgrado = document.getElementById('grado_idgrado').options[document.getElementById('grado_idgrado').selectedIndex].value;
            var idgrupo = document.getElementById('grupo_idgrupo').options[document.getElementById('grupo_idgrupo').selectedIndex].value;
            go2to('Calificar/inscrito.jsp?idgrupo='+idgrupo+'&idgrado='+idgrado+'&idescuela='+idescuela+'&idexamen='+idexamen,'alumnos');
        }catch(e){
            close('alumnos');
        }
    }
    function showQuestions(idinscrito){
        try{
            go2to('Calificar/calificar.jsp?idinscrito='+idinscrito,'calificar');
        }catch(e){
            close('calificar');
        }
    }
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Calificar Alumnos</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="form-group">
                    <label for="examen_idexamen">Examen:</label>
                    <select class="form-control" id="examen_idexamen" onchange="javascript:update();">
                        <%for(beans.Examen examen:examenfacade.getAll()){%>
                            <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                        <%}%>
                    </select>
                </div>
            </div>
        </div>
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="form-group">
                    <label for="escuela_idescuela">Escuela:</label>
                    <select class="form-control" id="escuela_idescuela" onchange="javascript:update();">
                        <%java.util.List<beans.Escuela> escuelas = new java.util.ArrayList();
                        if(session.getAttribute("usuario")!=null){
                            beans.Usuario u = (beans.Usuario)session.getAttribute("usuario");
                            if(u.getNivel()>1){
                                escuelas = escuelafacade.getAll();
                            }else{
                                escuelas = escuelafacade.getByAsignacionUsuario(u.getIdusuario());
                            }
                        }
                        for(beans.Escuela escuela:escuelas){%>
                            <option value="<%=escuela.getIdescuela()%>"><%=escuela.getNombre()%></option>
                        <%}%>
                    </select>
                </div>
                <div class="form-group">
                    <label for="grado_idgrado">Grado:</label>
                    <select class="form-control" id="grado_idgrado" onchange="javascript:update();">
                        <%for(beans.Grado grado:gradofacade.getAll()){%>
                            <option value="<%=grado.getIdgrado()%>"><%=grado.getNombre()%></option>
                        <%}%>
                    </select>
                </div>
                <div class="form-group">
                    <label for="grupo_idgrupo">Grupo:</label>
                    <select class="form-control" id="grupo_idgrupo" onchange="javascript:update();">
                        <%for(beans.Grupo grupo:grupofacade.getAll()){%>
                            <option value="<%=grupo.getIdgrupo()%>"><%=grupo.getNombre()%></option>
                        <%}%>
                    </select>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body" id="alumnos"></div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
        <div class="panel panel-red" id="calificar"></div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
                            
<script>
    update();
</script>

<%
escuelafacade.close();
examenfacade.close();
gradofacade.close();
grupofacade.close();
%>