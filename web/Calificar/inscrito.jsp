<%-- 
    Document   : inscrito
    Created on : 2/02/2015, 12:11:04 AM
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.InscritoFacade"/>
<jsp:useBean id="efacade" scope="page" class="facades.ExamenFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.AlumnoFacade"/>
<jsp:useBean id="esfacade" scope="page" class="facades.EstatusFacade"/>
<jsp:useBean id="escfacade" scope="page" class="facades.EscuelaFacade"/>

<%
int idexamen = 0;
if(request.getParameter("idexamen")!=null){
    idexamen = Integer.parseInt(request.getParameter("idexamen"));
}
int idescuela = 0;
if(request.getParameter("idescuela")!=null){
    idescuela = Integer.parseInt(request.getParameter("idescuela"));
}
int idgrado = 0;
if(request.getParameter("idgrado")!=null){
    idgrado = Integer.parseInt(request.getParameter("idgrado"));
}
int idgrupo = 0;
if(request.getParameter("idgrupo")!=null){
    idgrupo = Integer.parseInt(request.getParameter("idgrupo"));
}
long totalPreguntas = (Long)efacade.getTotalPreguntas(idexamen,idgrado);

boolean show = true;
beans.Estatus estatus = esfacade.getByExamenEscuela(idexamen, idescuela);
if(estatus.getIdestatus()!=0){
    if(estatus.getCalificacion()!=0||estatus.getInscripcion()==0){
        show = false;
    }
}else{
    show = false;
}%>

<form role="form" name="formsearch" id="formsearch" action="inscrito.jsp" method="post">
    <div class="table-responsive">
        <table class="table table-striped table-condensed table-hover" id="dataTables-example">
            <thead>
                <tr>
                    <th class="center-col-lg-1">Estatus</th>
                    <th class="col-lg-2">C.U.R.P.</th>
                    <th class="col-lg-2">A. Paterno</th>
                    <th class="col-lg-2">A. Materno</th>
                    <th class="col-lg-3">Nombre(s)</th>
                    <th class="center-col-lg-1">Comandos</th>
                </tr>
            </thead>
            <tbody>
                <%for(beans.Inscrito i:facade.getByParams(idexamen,idescuela,idgrado,idgrupo)){
                    beans.Alumno a = afacade.getById(i.getAlumno_idalumno());
                    long contestadas = (Long)facade.getContestadas(i.getIdinscrito());%>
                    <tr>
                        <td class="center-col-lg-1">
                            <%String color = "";
                            String mensaje = "";
                            if(contestadas==totalPreguntas){
                                color = "success";
                                mensaje = "OK";
                            }else{
                                if(contestadas<totalPreguntas){
                                    color = "danger";
                                    mensaje = contestadas+" / "+totalPreguntas;
                                }else{
                                    color = "primary";
                                    mensaje = contestadas+" / "+totalPreguntas;
                                }
                            }%>
                            <button type="button" class="btn btn-<%=color%> btn-xs"><%=mensaje%></button>
                        </td>
                        <td><%=a.getCurp()%></td>
                        <td><%=a.getPaterno()%></td>
                        <td><%=a.getMaterno()%></td>
                        <td><%=a.getNombre()%></td>
                        <td class="center-col-lg-1">                            
                            <%if(show){%>
                                <button type="button" class="btn btn-warning btn-xs" onclick="javascript:showQuestions(<%=i.getIdinscrito()%>);"><i class="fa fa-check-square-o"></i></button>
                            <%}else{%>
                                <button type="button" class="btn btn-default btn-xs" DISABLED><i class="fa fa-lock"></i></button>
                            <%}%>
                        </td>
                    </tr>
                <%}if(show){
                    beans.Escuela escuela = escfacade.getById(idescuela);%>
                    <tr>
                        <td colspan="6" class="center-col-lg-12">
                            <button type="button" class="btn btn-danger btn-xs" onclick="javascript:save('Calificar/cerrar.jsp?idexamen=<%=idexamen%>&idescuela=<%=idescuela%>','Calificar/divlistado.jsp','formsearch','page-wrapper');">CERRAR CALIFICACIONES EN [<%=escuela.getNombre()%>]</button>
                        </td>
                    </tr>
                <%}else{%>
                    <tr>
                        <td colspan="6" class="center-col-lg-12">
                            <button type="button" class="btn btn-default btn-xs" DISABLED>CALIFICACIONES CERRADAS</button>
                        </td>
                    </tr>
                <%}%>
            </tbody>
        </table>
        <!-- /. div-filters -->
    </div>
    <!-- /.table-responsive -->
</form>
            
<script>
    close('calificar');
</script>
            
<%
facade.close();
afacade.close();
efacade.close();
esfacade.close();
escfacade.close();
%>