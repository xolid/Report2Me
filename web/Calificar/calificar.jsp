<%-- 
    Document   : inscribir
    Created on : 2/02/2015, 09:19:11 AM
    Author     : solid
--%>

<jsp:useBean id="alumno" class="beans.Alumno" scope="page"/>
<jsp:useBean id="inscrito" class="beans.Inscrito" scope="page"/>

<jsp:useBean id="ifacade" scope="page" class="facades.InscritoFacade"/>
<jsp:useBean id="efacade" scope="page" class="facades.EvaluacionFacade"/>
<jsp:useBean id="mfacade" scope="page" class="facades.MateriaFacade"/>
<jsp:useBean id="pfacade" scope="page" class="facades.PreguntaFacade"/>
<jsp:useBean id="rfacade" scope="page" class="facades.RespuestaFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.AlumnoFacade"/>
<jsp:useBean id="esfacade" scope="page" class="facades.EstatusFacade"/>

<%
if(request.getParameter("accion")!=null){
    int idinscrito = Integer.parseInt(request.getParameter("idinscrito"));
    inscrito = ifacade.getById(idinscrito);
    if(request.getParameter("accion").equals("1")){
        for(beans.Evaluacion evaluacion:efacade.getByExamenGrado(inscrito.getExamen_idexamen(),inscrito.getGrado_idgrado())){
            for(beans.Pregunta pregunta:pfacade.getByEvaluacion(evaluacion.getIdevaluacion())){
                String value = request.getParameter("pregunta_"+pregunta.getIdpregunta());
                beans.Respuesta respuesta = rfacade.getByInscritoPregunta(inscrito.getIdinscrito(),pregunta.getIdpregunta());
                if(respuesta.getIdrespuesta()!=0){
                    respuesta.setOpcion(Integer.parseInt(value));
                }
                else{
                    respuesta.setIdrespuesta(0);
                    respuesta.setOpcion(Integer.parseInt(value));
                    respuesta.setPregunta_idpregunta(pregunta.getIdpregunta());
                    respuesta.setInscrito_idinscrito(inscrito.getIdinscrito());
                }
                rfacade.salvarRespuesta(respuesta);
            }
        }
    }
    if(request.getParameter("accion").equals("2")){
        rfacade.deleteAllbyInscrito(inscrito.getIdinscrito());
    }
}
java.util.List<beans.Respuesta> respuestas = null;
if(request.getParameter("idinscrito")!=null){
    int idinscrito = Integer.parseInt(request.getParameter("idinscrito"));
    inscrito = ifacade.getById(idinscrito);
    respuestas = rfacade.getByInscrito(inscrito.getIdinscrito());
    alumno = afacade.getById(inscrito.getAlumno_idalumno());
}
boolean show = true;
beans.Estatus estatus = esfacade.getByExamenEscuela(inscrito.getExamen_idexamen(),inscrito.getEscuela_idescuela());
if(estatus.getIdestatus()!=0){
    if(estatus.getCalificacion()!=0){
        show = false;
    }
}
%>

<div class="panel-heading">
    Alumno: [<%=alumno.getPaterno()+" "+alumno.getMaterno()+" "+alumno.getNombre()%>]
</div>
<!-- /.panel-heading -->
<div class="panel-body">
    <div class="row">
        <%if(show){%>
            <form role="form" id="formcalificar" name="formcalificar" action="calificar.jsp" method="post">
                <input type="hidden" id="idinscrito" name="idinscrito" value="<%=inscrito.getIdinscrito()%>"/>
                <div class="col-lg-12">
                    <%for(beans.Evaluacion evaluacion:efacade.getByExamenGrado(inscrito.getExamen_idexamen(),inscrito.getGrado_idgrado())){
                        beans.Materia materia = mfacade.getById(evaluacion.getMateria_idmateria());%>
                        <h1 class="page-header"><%=materia.getNombre()%></h1>
                        <%for(beans.Pregunta pregunta:pfacade.getByEvaluacion(evaluacion.getIdevaluacion())){
                            int seleccionada = -1;
                            if(respuestas!=null){
                                for(beans.Respuesta r:respuestas){
                                    if(r.getPregunta_idpregunta()==pregunta.getIdpregunta()){
                                        seleccionada = r.getOpcion();
                                    }
                                }
                            }%>    
                            <div class="form-group">
                                <label>Pregunta <%=pregunta.getIndice()%>&emsp;&emsp;&emsp;</label>
                                <%for(int i=0;i<pregunta.getOpciones();i++){%>
                                    <label class="radio-inline">
                                        <input type="radio" name="pregunta_<%=pregunta.getIdpregunta()%>" id="pregunta_<%=pregunta.getIdpregunta()+"_"+i%>" value="<%=i%>" <%=(seleccionada==i?"CHECKED":"")%> required><%=((char)(i+65))%>&emsp;
                                    </label>
                                <%}%>
                            </div>
                        <%}
                    }%>
                    <button class="btn btn-success" type="button" onclick="javascript:save('Calificar/calificar.jsp','Calificar/inscrito.jsp?idexamen=<%=inscrito.getExamen_idexamen()%>&idescuela=<%=inscrito.getEscuela_idescuela()%>&idgrado=<%=inscrito.getGrado_idgrado()%>&idgrupo=<%=inscrito.getGrupo_idgrupo()%>','formcalificar','alumnos');">Guardar</button>
                    <button class="btn btn-danger" type="button" onclick="javascript:erase('Calificar/calificar.jsp','Calificar/inscrito.jsp?idexamen=<%=inscrito.getExamen_idexamen()%>&idescuela=<%=inscrito.getEscuela_idescuela()%>&idgrado=<%=inscrito.getGrado_idgrado()%>&idgrupo=<%=inscrito.getGrupo_idgrupo()%>','formcalificar','alumnos');">Borrar</button>
                </div>
                <!-- /.col-lg-12 (nested) -->
            </form>                
            <!-- /.form -->    
        <%}else{%>
            <div class="col-lg-12">
                <button type="button" class="btn btn-default btn-lg btn-block" DISABLED>La Escuela ha Terminado la Calificacion del Examen</button>
            </div>
        <%}%>
    </div>
    <!-- /.row (nested) -->
</div>
<!-- /.panel-body -->

<%
ifacade.close();
efacade.close();
mfacade.close();
pfacade.close();
afacade.close();
rfacade.close();
esfacade.close();
%>
