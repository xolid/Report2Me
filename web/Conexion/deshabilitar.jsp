<%-- 
    Document   : cerrar
    Created on : Nov 4, 2014, 10:07:32 PM
    Author     : CarlosAlberto
--%>

<%@ page import="servlets.ServletListener"%>
<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>

<%String id = "";
if(request.getParameter("id")!=null){
    id = request.getParameter("id");
}
for(javax.servlet.http.HttpSession s:servlets.ServletListener.getSessions()){
    if(s.getId().equals(id)){
        beans.Usuario usuario = (beans.Usuario)s.getAttribute("usuario");
        usuario.setFegreso(new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date()));
        facade.salvarUsuario(usuario);        
        s.invalidate();
    }
}
facade.close();
%>
