<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Conexiones</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Conexiones Activas en el Sistema
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-3">Nombre</th>
                                <th class="col-lg-3">Nivel</th>
                                <th class="col-lg-3">ID</th>
                                <th class="col-lg-2">Inicio de Sesion</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(javax.servlet.http.HttpSession s:servlets.ServletListener.getSessions()){
                                beans.Usuario u = (beans.Usuario)s.getAttribute("usuario");
                                if(u!=null){%>
                                    <tr>
                                        <td><%=u.getNombre()%></td>
                                        <td><%=new auxiliares.Niveles().getNivelName(u.getNivel())%></td>
                                        <td><%=s.getId()%></td>
                                        <td><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(s.getCreationTime()))%></td>
                                        <td class="center-col-lg-2">
                                            <button class="btn btn-danger btn-xs" type="button" onclick="javascript:executeRedirec('Conexion/deshabilitar.jsp','id=<%=s.getId()%>','Conexion/divlistado.jsp','page-wrapper');"><i class="fa fa-eraser"></i></button>
                                            <button class="btn btn-warning btn-xs" type="button" onclick="javascript:executeRedirec('Conexion/cerrar.jsp','id=<%=s.getId()%>','Conexion/divlistado.jsp','page-wrapper');"><i class="fa fa-stop"></i></button>
                                        </td>
                                    </tr>
                                <%}else{%>
                                    <tr>
                                        <td>Sesion no logeada</td>
                                        <td>Acceso no permitido</td>
                                        <td><%=s.getId()%></td>
                                        <td><%=new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(s.getCreationTime()))%></td>
                                        <td class="center-col-lg-2">
                                            <button class="btn btn-warning btn-xs" type="button" onclick="javascript:executeRedirec('Conexion/cerrar.jsp','id=<%=s.getId()%>','Conexion/divlistado.jsp','page-wrapper');"><i class="fa fa-stop"></i></button>
                                        </td>
                                    </tr>
                                <%}
                            }%>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
                        
<%
facade.close();
%>