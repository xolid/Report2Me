<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<jsp:useBean id="examen" class="beans.Examen" scope="page"/>
<jsp:setProperty name="examen" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.ExamenFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarExamen(examen);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarExamen(examen);        
    }
}
if(request.getParameter("id")!=null){
    int idexamen=Integer.parseInt((String)request.getParameter("id"));
    examen=facade.getById(idexamen);
}else{
    String date = new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
    String month = new java.text.SimpleDateFormat("MMMM").format(new java.util.Date()).toUpperCase();
    String year = new java.text.SimpleDateFormat("yyyy").format(new java.util.Date());
    examen.setIdexamen(0);
    examen.setNombre(month+" "+year);
    examen.setFecha(date);
}
%>

<script>
    function changeDate(){
        var date1 = document.getElementById("fecha").value;
        var array1 = date1.split("-");
        var value = toNameMonth(array1[1])+" "+array1[0];
        document.getElementById("nombre").value = value;
    }
    function toNameMonth(numMonth){
        if(numMonth === "01"){
            return "ENERO";
        }
        if(numMonth === "02"){
            return "FEBRERO";
        }
        if(numMonth === "03"){
            return "MARZO";
        }
        if(numMonth === "04"){
            return "ABRIL";
        }
        if(numMonth === "05"){
            return "MAYO";
        }
        if(numMonth === "06"){
            return "JUNIO";
        }
        if(numMonth === "07"){
            return "JULIO";
        }
        if(numMonth === "08"){
            return "AGOSTO";
        }
        if(numMonth === "09"){
            return "SEPTIEMBRE";
        }
        if(numMonth === "10"){
            return "OCTUBRE";
        }
        if(numMonth === "11"){
            return "NOVIEMBRE";
        }
        if(numMonth === "12"){
            return "DICIEMBRE";
        }
        return "undefined";
    }
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Examen</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos del Examen
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="examen.jsp" method="post">
                        <input type="hidden" id="idexamen" name="idexamen" value="<%=examen.getIdexamen()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" maxlength="50" value="<%=examen.getNombre()%>" disabled required>
                            </div>
                            <div class="form-group">
                                <label for="fecha">Fecha</label>
                                <input class="form-control" id="fecha" name="fecha" type="date" value="<%=examen.getFecha()%>" onchange="javascript:changeDate();" required>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Examen/examen.jsp','Examen/divlistado.jsp','form1','page-wrapper');">Guardar</button>
                            <%if(examen.getIdexamen()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Examen/examen.jsp','Examen/divlistado.jsp','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Examen/divlistado.jsp','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6"></div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>