<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="facade" scope="page" class="facades.ExamenFacade"/>

<%int index=0;
if(request.getParameter("index")!=null){
    index=Integer.parseInt(request.getParameter("index"));
}
String campo="nombre";
if(request.getParameter("campo")!=null){
    campo=request.getParameter("campo");
}
int size=10;
long total=(Long)facade.getAllTotal();
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Examen</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="form-group">
                    <label for="orden">Ordenar por:</label>
                    <select class="form-control" id="ordenador" onchange="javascript:go2to('Examen/divlistado.jsp?index=<%=index%>&campo='+document.getElementById('ordenador').options[document.getElementById('ordenador').selectedIndex].value,'page-wrapper');">
                        <option value="nombre" <%=campo.equals("nombre")?"selected":""%>>Nombre</option>
                        <option value="fecha" <%=campo.equals("fecha")?"selected":""%>>Fecha</option>
                    </select>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-6">Nombre</th>
                                <th class="col-lg-5">Fecha</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(beans.Examen e:facade.getAllLimitedOrdered(index, size, campo)){%>
                                <tr>
                                    <td><%=e.getNombre()%></td>
                                    <td><%=e.getFecha()%></td>
                                    <td class="center-col-lg-1">
                                        <button type="button" class="btn btn-default btn-xs" onclick="javascript:go2to('Evaluacion/divlistado.jsp?examen=<%=e.getIdexamen()%>','page-wrapper')"><i class="fa fa-arrow-circle-down"></i></button>
                                        <button type="button" class="btn btn-warning btn-xs" onclick="javascript:go2to('Examen/examen.jsp?id=<%=e.getIdexamen()%>','page-wrapper')"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <%}%>
                            <tr>
                                <td colspan="2"</td>
                                <td class="center-col-lg-1">
                                    <button type="button" class="btn btn-success btn-xs" onclick="javascript:go2to('Examen/examen.jsp','page-wrapper');"><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                    <div class="col-lg-12 col-center">
                        <ul class="pagination">
                            <%if(index+size<=total){%>
                                <li class="next"><a href="#" onclick="javascript:go2to('Examen/divlistado.jsp?index=<%=index+size%>&campo='+document.getElementById('ordenador').options[document.getElementById('ordenador').selectedIndex].value,'page-wrapper');">>></a></li>
                            <%}else{%>
                                <li class="next disabled"><a>>></a></li>
                            <%}%>
                            <li><a>Pagina <%=(index/size+1)+" de "+(total/size+1)%></a></li>
                            <%if(index-size>=0){%>
                                <li class="prev"><a href="#" onclick="javascript:go2to('Examen/divlistado.jsp?index=<%=index-size%>&campo='+document.getElementById('ordenador').options[document.getElementById('ordenador').selectedIndex].value,'page-wrapper');"><<</a></li>
                            <%}else{%>
                                <li class="prev disabled"><a href="#"><<</a></li>
                            <%}%>
                        </ul>
                    </div>
                    <!-- /. div-pagination -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<%
facade.close();
%>