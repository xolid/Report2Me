<%-- 
    Document   : guardar
    Created on : 2/02/2015, 10:21:50 AM
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.InscritoFacade"/>

<%
int idexamen = 0;
if(request.getParameter("idexamen")!=null){
    idexamen = Integer.parseInt(request.getParameter("idexamen"));
}
int idescuela = 0;
if(request.getParameter("idescuela")!=null){
    idescuela = Integer.parseInt(request.getParameter("idescuela"));
}
int idgrado = 0;
if(request.getParameter("idgrado")!=null){
    idgrado = Integer.parseInt(request.getParameter("idgrado"));
}
int idgrupo = 0;
if(request.getParameter("idgrupo")!=null){
    idgrupo = Integer.parseInt(request.getParameter("idgrupo"));
}
int idalumno = 0;
if(request.getParameter("idalumno")!=null){
    idalumno = Integer.parseInt(request.getParameter("idalumno"));
}
if(idexamen!=0&&idescuela!=0&&idgrupo!=0&&idalumno!=0){
    beans.Inscrito inscrito = new beans.Inscrito();
    inscrito.setIdinscrito(0);
    inscrito.setExamen_idexamen(idexamen);
    inscrito.setEscuela_idescuela(idescuela);
    inscrito.setGrado_idgrado(idgrado);
    inscrito.setGrupo_idgrupo(idgrupo);
    inscrito.setAlumno_idalumno(idalumno);
    facade.salvarInscrito(inscrito);
}

facade.close();
%>