<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<jsp:useBean id="alumno" class="beans.Alumno" scope="page"/>
<jsp:setProperty name="alumno" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.AlumnoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarAlumno(alumno);
    }
}
int idgrupo = 0;
if(request.getParameter("idgrupo")!=null){
    idgrupo = Integer.parseInt(request.getParameter("idgrupo"));
}
int idmateria = 0;
if(request.getParameter("idmateria")!=null){
    idmateria = Integer.parseInt(request.getParameter("idmateria"));
}
int idciclo = 0;
if(request.getParameter("idciclo")!=null){
    idciclo = Integer.parseInt(request.getParameter("idciclo"));
}
alumno.setIdalumno(0);
alumno.setCurp("");
alumno.setPaterno("");
alumno.setMaterno("");
alumno.setNombre("");
alumno.setFnacimiento("");
%>

<div class="row">
    <form role="form" id="form1" name="form1" action="alumno.jsp" method="post">
        <input type="hidden" id="idalumno" name="idalumno" value="<%=alumno.getIdalumno()%>"/>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="curp">C.U.R.P.</label>
                <input class="form-control" id="curp" name="curp" type="text" placeholder="C.U.R.P." maxlength="25" value="<%=alumno.getCurp()%>" pattern="[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$" autofocus required tabindex="1">
            </div>
            <div class="form-group">
                <label for="materno">A. Materno</label>
                <input class="form-control" id="materno" name="materno" type="text" placeholder="Apellido Materno" value="<%=alumno.getMaterno()%>" required tabindex="3">
            </div>
            <div class="form-group">
                <label>Fecha de Nacimiento</label>
                <input class="form-control" id="fnacimiento" name="fnacimiento" type="date" value="<%=alumno.getFnacimiento()%>" required tabindex="5">
            </div>
            <button class="btn btn-success" type="button" onclick="javascript:save('Inscrito/alumno.jsp','Inscrito/inscribir.jsp?idciclo=<%=idciclo%>&idmateria=<%=idmateria%>&idgrupo=<%=idgrupo%>','form1','inscribir');">Guardar</button>
            <button class="btn btn-default" type="button" onclick="javascript:go2to('Inscrito/inscribir.jsp?idciclo=<%=idciclo%>&idmateria=<%=idmateria%>&idgrupo=<%=idgrupo%>','inscribir');">Volver</button>                            
        </div>
        <!-- /.col-lg-6 (nested) -->
        <div class="col-lg-6">
            <div class="form-group">
                <label for="paterno">A. Paterno</label>
                <input class="form-control" id="paterno" name="paterno" type="text" placeholder="Apellido Paterno" value="<%=alumno.getPaterno()%>" required tabindex="2">
            </div>
            <div class="form-group">
                <label for="nombre">Nombre(s)</label>
                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" value="<%=alumno.getNombre()%>" required tabindex="4">
            </div>
        </div>
        <!-- /.col-lg-6 (nested) -->
    </form>                
    <!-- /.form -->    
</div>
<!-- /.row (nested) -->

<%
facade.close();
%>