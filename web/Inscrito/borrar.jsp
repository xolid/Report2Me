<%-- 
    Document   : guardar
    Created on : 2/02/2015, 10:21:50 AM
    Author     : solid
--%>

<jsp:useBean id="inscrito" class="beans.Inscrito" scope="page"/>
<jsp:useBean id="facade" scope="page" class="facades.InscritoFacade"/>

<%
if(request.getParameter("id")!=null){
    int idinscrito = Integer.parseInt(request.getParameter("id"));
    inscrito = facade.getById(idinscrito);
}
if(inscrito.getIdinscrito()!=0){
    facade.borrarInscrito(inscrito);
}

facade.close();
%>

