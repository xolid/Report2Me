<%-- 
    Document   : inscribir
    Created on : 2/02/2015, 09:19:11 AM
    Author     : solid
--%>

<jsp:useBean id="alumno" class="beans.Alumno" scope="page"/>
<jsp:useBean id="facade" scope="page" class="facades.AlumnoFacade"/>
<jsp:useBean id="efacade" scope="page" class="facades.EstatusFacade"/>

<%
int idexamen = 0;
if(request.getParameter("idexamen")!=null){
    idexamen = Integer.parseInt(request.getParameter("idexamen"));
}
int idescuela = 0;
if(request.getParameter("idescuela")!=null){
    idescuela = Integer.parseInt(request.getParameter("idescuela"));
}
int idgrado = 0;
if(request.getParameter("idgrado")!=null){
    idgrado = Integer.parseInt(request.getParameter("idgrado"));
}
int idgrupo = 0;
if(request.getParameter("idgrupo")!=null){
    idgrupo = Integer.parseInt(request.getParameter("idgrupo"));
}

boolean show = true;
beans.Estatus estatus = efacade.getByExamenEscuela(idexamen, idescuela);
if(estatus.getIdestatus()!=0){
    if(estatus.getInscripcion()!=0){
        show = false;
    }
}

if(request.getParameter("curp")!=null){
    String curp = request.getParameter("curp");
    alumno = facade.getByCurp(curp);
    if(alumno.getIdalumno()==0){
        alumno.setIdalumno(0);
        alumno.setCurp("");
        alumno.setPaterno("");
        alumno.setMaterno("");
        alumno.setNombre("");
        alumno.setFnacimiento("");
    }
}else{
    alumno.setIdalumno(0);
    alumno.setCurp("");
    alumno.setPaterno("");
    alumno.setMaterno("");
    alumno.setNombre("");
    alumno.setFnacimiento("");
}
%>

<div class="row">
    <%if(show){%>
        <form role="form" id="formsearch" name="formsearch" action="inscribir.jsp" method="post">
            <input type="hidden" id="idexamen" name="idexamen" value="<%=idexamen%>"/>
            <input type="hidden" id="idescuela" name="idescuela" value="<%=idescuela%>"/>
            <input type="hidden" id="idgrado" name="idgrado" value="<%=idgrado%>"/>
            <input type="hidden" id="idgrupo" name="idgrupo" value="<%=idgrupo%>"/>
            <input type="hidden" id="idalumno" name="idalumno" value="<%=alumno.getIdalumno()%>"/>
            <div class="col-lg-2">
                <div class="form-group">
                    <label for="curp">C.U.R.P.</label>
                    <input class="form-control" id="curp" name="curp" type="text" placeholder="C.U.R.P." value="<%=alumno.getCurp()%>" autofocus>
                </div>
                <button class="btn btn-default" type="button" onclick="javascript:search('Inscrito/inscribir.jsp','formsearch','inscribir');">Buscar</button>
                <%if(alumno.getIdalumno()!=0){%>
                    <button class="btn btn-success" type="button" onclick="javascript:save('Inscrito/guardar.jsp','Inscrito/inscrito.jsp?idexamen=<%=idexamen%>&idescuela=<%=idescuela%>&idgrado=<%=idgrado%>&idgrupo=<%=idgrupo%>','formsearch','alumnos');">Inscribir</button>
                <%}else{%>
                    <button class="btn btn-warning" type="button" onclick="javascript:go2to('Inscrito/alumno.jsp?Inscrito/inscrito.jsp?idexamen=<%=idexamen%>&idescuela=<%=idescuela%>&idgrado=<%=idgrado%>&idgrupo=<%=idgrupo%>','inscribir');">Nuevo</button>
                <%}%>
            </div>
            <!-- /.col-lg-6 (nested) -->
            <div class="col-lg-2">
                <div class="form-group">
                    <label for="paterno">A. Paterno</label>
                    <input class="form-control" id="paterno" name="paterno" type="text" placeholder="A. Paterno" value="<%=alumno.getPaterno()%>" disabled>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group">
                    <label for="materno">A. Materno</label>
                    <input class="form-control" id="materno" name="materno" type="text" placeholder="A. Materno" value="<%=alumno.getMaterno()%>" disabled>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" value="<%=alumno.getNombre()%>" disabled>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="form-group">
                    <label for="fnacimiento">F. Nacimiento</label>
                    <input class="form-control" id="fnacimiento" name="fnacimiento" type="text" placeholder="F. Nacimiento" value="<%=alumno.getFnacimiento()%>" disabled>
                </div>
            </div>
            <!-- /.col-lg-6 (nested) -->
        </form>                
        <!-- /.form -->    
    <%}else{%>
        <div class="col-lg-12">
            <button type="button" class="btn btn-default btn-lg btn-block" DISABLED>La Escuela ha Terminado la Inscripcion de Alumnos</button>
        </div>
    <%}%>
</div>
<!-- /.row (nested) -->

<%
facade.close();
efacade.close();
%>