<%-- 
    Document   : guardar
    Created on : 2/02/2015, 10:21:50 AM
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.EstatusFacade"/>

<%
if(request.getParameter("idexamen")!=null&&request.getParameter("idescuela")!=null){
    int idexamen = Integer.parseInt(request.getParameter("idexamen"));
    int idescuela = Integer.parseInt(request.getParameter("idescuela"));
    beans.Estatus estatus = facade.getByExamenEscuela(idexamen, idescuela);
    if(estatus.getIdestatus()!=0){
        estatus.setInscripcion(1);
    }else{
        estatus.setIdestatus(0);
        estatus.setInscripcion(1);
        estatus.setCalificacion(0);
        estatus.setExamen_idexamen(idexamen);
        estatus.setEscuela_idescuela(idescuela);
    }
    facade.salvarEstatus(estatus);
}
facade.close();
%>