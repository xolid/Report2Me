<%-- 
    Document   : inscrito
    Created on : 2/02/2015, 12:11:04 AM
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.InscritoFacade"/>
<jsp:useBean id="afacade" scope="page" class="facades.AlumnoFacade"/>
<jsp:useBean id="efacade" scope="page" class="facades.EstatusFacade"/>
<jsp:useBean id="esfacade" scope="page" class="facades.EscuelaFacade"/>

<%
int idexamen = 0;
if(request.getParameter("idexamen")!=null){
    idexamen = Integer.parseInt(request.getParameter("idexamen"));
}
int idescuela = 0;
if(request.getParameter("idescuela")!=null){
    idescuela = Integer.parseInt(request.getParameter("idescuela"));
}
int idgrado = 0;
if(request.getParameter("idgrado")!=null){
    idgrado = Integer.parseInt(request.getParameter("idgrado"));
}
int idgrupo = 0;
if(request.getParameter("idgrupo")!=null){
    idgrupo = Integer.parseInt(request.getParameter("idgrupo"));
}

boolean show = true;
beans.Estatus estatus = efacade.getByExamenEscuela(idexamen, idescuela);
if(estatus.getIdestatus()!=0){
    if(estatus.getInscripcion()!=0){
        show = false;
    }
}%>

<div class="table-responsive">
    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th class="col-lg-2">C.U.R.P.</th>
                <th class="col-lg-3">A. Paterno</th>
                <th class="col-lg-3">A. Materno</th>
                <th class="col-lg-3">Nombre(s)</th>
                <th class="center-col-lg-1">Comandos</th>
            </tr>
        </thead>
        <tbody>
            <%for(beans.Inscrito i:facade.getByParams(idexamen,idescuela,idgrado,idgrupo)){
                beans.Alumno a = afacade.getById(i.getAlumno_idalumno());%>
                <tr>
                    <td><%=a.getCurp()%></td>
                    <td><%=a.getPaterno()%></td>
                    <td><%=a.getMaterno()%></td>
                    <td><%=a.getNombre()%></td>
                    <td class="center-col-lg-1">
                        <%if(show){%>
                            <button type="button" class="btn btn-danger btn-xs" onclick="javascript:erase('Inscrito/borrar.jsp?id=<%=i.getIdinscrito()%>','Inscrito/inscrito.jsp?idexamen=<%=idexamen%>&idescuela=<%=idescuela%>&idgrado=<%=idgrado%>&idgrupo=<%=idgrupo%>','formsearch','alumnos');"><i class="fa fa-eraser"></i></button>
                        <%}else{%>
                            <button type="button" class="btn btn-default btn-xs" DISABLED><i class="fa fa-lock"></i></button>
                        <%}%>
                    </td>
                </tr>
            <%}
            if(show){
                beans.Escuela escuela = esfacade.getById(idescuela);%>
                <tr>
                    <td colspan="5" class="center-col-lg-12">
                        <button type="button" class="btn btn-danger btn-xs" onclick="javascript:save('Inscrito/cerrar.jsp?idexamen=<%=idexamen%>&idescuela=<%=idescuela%>','Inscrito/divlistado.jsp','formsearch','page-wrapper');">CERRAR INSCRIPCIONES EN [<%=escuela.getNombre()%>]</button>
                    </td>
                </tr>
            <%}else{%>
                <tr>
                    <td colspan="5" class="center-col-lg-12">
                        <button type="button" class="btn btn-default btn-xs" DISABLED>INSCRIPCIONES CERRADAS</button>
                    </td>
                </tr>
            <%}%>
        </tbody>
    </table>
    <!-- /. div-filters -->
</div>
<!-- /.table-responsive -->

<%
facade.close();
afacade.close();
efacade.close();
%>