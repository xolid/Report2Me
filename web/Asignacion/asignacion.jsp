<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<jsp:useBean id="asignacion" class="beans.Asignacion" scope="page"/>
<jsp:setProperty name="asignacion" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.AsignacionFacade"/>

<jsp:useBean id="ufacade" scope="page" class="facades.UsuarioFacade"/>
<jsp:useBean id="efacade" scope="page" class="facades.EscuelaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarAsignacion(asignacion);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarAsignacion(asignacion);        
    }
}
if(request.getParameter("id")!=null){
    int idasignacion=Integer.parseInt((String)request.getParameter("id"));
    asignacion=facade.getById(idasignacion);
}else{
    asignacion.setIdasignacion(0);
    asignacion.setUsuario_idusuario(0);
    asignacion.setEscuela_idescuela(0);
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Asignacion</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos de la Asignacion
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="asignacion.jsp" method="post">
                        <input type="hidden" id="idasignacion" name="idasignacion" value="<%=asignacion.getIdasignacion()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="usuario_idusuario">Usuario:</label>
                                <select class="form-control" name="usuario_idusuario" id="usuario_idusuario">
                                    <%for(beans.Usuario usuario:ufacade.getByNivel(1)){%>
                                        <option value="<%=usuario.getIdusuario()%>" <%=asignacion.getUsuario_idusuario()==usuario.getIdusuario()?"SELECTED":""%>><%=usuario.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Asignacion/asignacion.jsp','Asignacion/divlistado.jsp','form1','page-wrapper');">Guardar</button>
                            <%if(asignacion.getIdasignacion()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Asignacion/asignacion.jsp','Asignacion/divlistado.jsp','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Asignacion/divlistado.jsp','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="escuela_idescuela">Escuela:</label>
                                <select class="form-control" name="escuela_idescuela" id="escuela_idescuela">
                                    <%for(beans.Escuela escuela:efacade.getAll()){%>
                                        <option value="<%=escuela.getIdescuela()%>" <%=asignacion.getEscuela_idescuela()==escuela.getIdescuela()?"SELECTED":""%>><%=escuela.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
ufacade.close();
efacade.close();
%>