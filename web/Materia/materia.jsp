<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<%@page import="java.io.File"%>
<%@page import="beans.Materia"%>
<%@page import="auxiliares.Nivel"%>
<jsp:useBean id="materia" class="beans.Materia" scope="page"/>
<jsp:setProperty name="materia" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.MateriaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarMateria(materia);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarMateria(materia);        
    }
}
if(request.getParameter("id")!=null){
    int idmateria=Integer.parseInt((String)request.getParameter("id"));
    materia=facade.getById(idmateria);
}else{
    materia.setIdmateria(0);
    materia.setNombre("");
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Materia</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos del Materia
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="materia.jsp" method="post">
                        <input type="hidden" id="idmateria" name="idmateria" value="<%=materia.getIdmateria()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" maxlength="50" value="<%=materia.getNombre()%>" autofocus required>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Materia/materia.jsp','Materia/divlistado.jsp','form1','page-wrapper');">Guardar</button>
                            <%if(materia.getIdmateria()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Materia/materia.jsp','Materia/divlistado.jsp','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Materia/divlistado.jsp','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6"></div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>