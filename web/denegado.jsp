<%-- 
    Document   : error
    Created on : Oct 10, 2014, 11:26:57 PM
    Author     : CarlosAlberto
--%>

<div class="row">
    <div class="col-lg-12">&nbsp;</div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-warning">
            <p><strong>Acceso denegado...</strong></p>
            <br>
            <p>Esta area esta restringida para el nivel de privilegios que tiene asignados.</p>
        </div>
    </div>
    <!-- /.col-lg-6 -->
</div>