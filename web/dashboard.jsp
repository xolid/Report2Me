<%-- 
    Document   : dashboard
    Created on : Sep 24, 2014, 6:19:58 PM
    Author     : CarlosAlberto
--%>

<jsp:useBean id="efacade" scope="page" class="facades.ExamenFacade"/>

<script>
    function update(){
        gotoEXEC('Panel/bloques.jsp?idciclo='+document.getElementById('ciclo_idciclo').options[document.getElementById('ciclo_idciclo').selectedIndex].value,'bloques','graphics();');
    }
    function graphics(){
        go2to('Panel/graficas.jsp?idciclo='+document.getElementById('ciclo_idciclo').options[document.getElementById('ciclo_idciclo').selectedIndex].value,'graficas');
    }
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <div class="form-group">
                <label for="ciclo_idciclo">Ciclo:</label>
                <select class="form-control" id="ciclo_idciclo" onchange="javascript:update();">
                    <%for(beans.Examen examen:efacade.getAll()){%>
                        <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                    <%}%>
                </select>
            </div>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" id="bloques"></div>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Graficas</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row" id="graficas"></div>
<!-- /.row -->

<script>
    update();
</script>

<%
efacade.close();
%>