<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="evaluacion" class="beans.Evaluacion" scope="page"/>
<jsp:useBean id="examen" class="beans.Examen" scope="page"/>
<jsp:useBean id="efacade" scope="page" class="facades.EvaluacionFacade"/>
<jsp:useBean id="gfacade" scope="page" class="facades.GradoFacade"/>
<jsp:useBean id="mfacade" scope="page" class="facades.MateriaFacade"/>
<jsp:useBean id="facade" scope="page" class="facades.PreguntaFacade"/>

<%if(request.getParameter("evaluacion")!=null){
    int idevaluacion=Integer.parseInt(request.getParameter("evaluacion"));
    evaluacion = efacade.getById(idevaluacion);  
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Pregunta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Listado de Preguntas Registradas en la Evaluacion [<%=gfacade.getById(evaluacion.getGrado_idgrado()).getNombre()%>] - [<%=mfacade.getById(evaluacion.getMateria_idmateria()).getNombre()%>]
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-5">Indice</th>
                                <th class="col-lg-6">Opciones</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(beans.Pregunta p:facade.getByEvaluacion(evaluacion.getIdevaluacion())){%>
                                <tr>
                                    <td><%=p.getIndice()%></td>
                                    <td><%=p.getOpciones()%></td>
                                    <td class="center-col-lg-1">
                                        <button type="button" class="btn btn-warning btn-xs" onclick="javascript:go2to('Pregunta/pregunta.jsp?id=<%=p.getIdpregunta()%>','page-wrapper')"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <%}%>
                            <tr>
                                <td colspan="2"></td>
                                <td class="center-col-lg-1">
                                    <button type="button" class="btn btn-success btn-xs" onclick="javascript:go2to('Pregunta/pregunta.jsp?evaluacion=<%=evaluacion.getIdevaluacion()%>','page-wrapper');"><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                    <div class="col-lg-12 col-center">
                        <ul class="pagination">
                            <li><a href="#" onclick="javascript:go2to('Evaluacion/divlistado.jsp?examen=<%=evaluacion.getExamen_idexamen()%>','page-wrapper');">Volver</a></li>
                        </ul>
                    </div>
                    <!-- /. div-pagination -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<%
facade.close();
efacade.close();
gfacade.close();
mfacade.close();
%>