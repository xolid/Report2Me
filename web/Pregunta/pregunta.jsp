<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<jsp:useBean id="pregunta" class="beans.Pregunta" scope="page"/>
<jsp:setProperty name="pregunta" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.PreguntaFacade"/>

<jsp:useBean id="evaluacion" class="beans.Evaluacion" scope="page"/>
<jsp:useBean id="efacade" scope="page" class="facades.EvaluacionFacade"/>
<jsp:useBean id="gfacade" scope="page" class="facades.GradoFacade"/>
<jsp:useBean id="mfacade" scope="page" class="facades.MateriaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarPregunta(pregunta);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarPregunta(pregunta);        
    }
}
if(request.getParameter("id")!=null){
    int idpregunta=Integer.parseInt((String)request.getParameter("id"));
    pregunta=facade.getById(idpregunta);
    evaluacion = efacade.getById(pregunta.getEvaluacion_idevaluacion());
}else{
    pregunta.setIdpregunta(0);
    pregunta.setIndice(1);
    pregunta.setOpciones(4);
    pregunta.setCorrecta(0);
    if(request.getParameter("evaluacion")!=null){
        int idevaluacion = Integer.parseInt(request.getParameter("evaluacion"));
        evaluacion = efacade.getById(idevaluacion);
    }
    pregunta.setEvaluacion_idevaluacion(evaluacion.getIdevaluacion());
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Pregunta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos de la Pregunta en la Evaluacion [<%=gfacade.getById(evaluacion.getGrado_idgrado()).getNombre()%>] - [<%=mfacade.getById(evaluacion.getMateria_idmateria()).getNombre()%>]
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="pregunta.jsp" method="post">
                        <input type="hidden" id="idpregunta" name="idpregunta" value="<%=pregunta.getIdpregunta()%>"/>
                        <input type="hidden" id="evaluacion_idevaluacion" name="evaluacion_idevaluacion" value="<%=pregunta.getEvaluacion_idevaluacion()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="indice">Indice de la Pregunta</label>
                                <input class="form-control" id="indice" name="indice" type="number" min="0" max="1000" step="1" placeholder="Indice" value="<%=pregunta.getIndice()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="correcta">Opcion Correcta</label>
                                <input class="form-control" id="correcta" name="correcta" type="number" min="0" max="10" step="1" placeholder="Indice" value="<%=pregunta.getCorrecta()%>" required>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Pregunta/pregunta.jsp','Pregunta/divlistado.jsp?evaluacion=<%=evaluacion.getIdevaluacion()%>','form1','page-wrapper');">Guardar</button>
                            <%if(pregunta.getIdpregunta()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Pregunta/pregunta.jsp','Pregunta/divlistado.jsp?evaluacion=<%=evaluacion.getIdevaluacion()%>','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Pregunta/divlistado.jsp?evaluacion=<%=evaluacion.getIdevaluacion()%>','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="Opciones">Cantidad de Opciones</label>
                                <input class="form-control" id="opciones" name="opciones" type="number" min="0" max="10" step="1"  placeholder="Cantidad de Opciones" value="<%=pregunta.getOpciones()%>" required>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
efacade.close();
gfacade.close();
mfacade.close();
%>