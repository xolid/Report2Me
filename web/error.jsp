<%-- 
    Document   : error
    Created on : Oct 10, 2014, 11:26:57 PM
    Author     : CarlosAlberto
--%>

<div class="row">
    <div class="col-lg-12">&nbsp;</div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-danger">
            <p><strong>Operacion no permitida...</strong></p>
            <br>
            <p>La operacion solicitada no esta permitida en el sistema.</p>
        </div>
    </div>
    <!-- /.col-lg-6 -->
</div>