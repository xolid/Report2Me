<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="subjefatura" class="beans.Subjefatura" scope="page"/>

<jsp:useBean id="sfacade" scope="page" class="facades.SubjefaturaFacade"/>
<jsp:useBean id="facade" scope="page" class="facades.SupervisionFacade"/>

<%
java.util.List<auxiliares.Navegacion> navegaciones = new java.util.ArrayList();
if(request.getParameter("subjefatura")!=null){
    int idsubjefatura=Integer.parseInt(request.getParameter("subjefatura"));
    subjefatura = sfacade.getById(idsubjefatura);
    
    if(session.getAttribute("navegaciones")!=null){
        navegaciones = (java.util.List<auxiliares.Navegacion>)session.getAttribute("navegaciones");
    }
    auxiliares.Navegacion navegacion = new auxiliares.Navegacion();
    navegacion.setUrl("Supervision/divlistado.jsp?subjefatura="+subjefatura.getIdsubjefatura());
    navegacion.setNombre(subjefatura.getNombre());
    if(navegaciones.size()>0){
        navegaciones=navegaciones.subList(0,1);
        navegaciones.set(0,navegacion);
    }else{
        navegaciones.add(navegacion);
    }
    session.setAttribute("navegaciones",navegaciones);
    
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Supervision</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Listado de Supervisiones Registradas de la Subjefatura [<%=subjefatura.getNombre()%>]
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-5">Nombre</th>
                                <th class="col-lg-3">Direccion</th>
                                <th class="col-lg-3">Responsable</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(beans.Supervision s:facade.getBySubjefatura(subjefatura.getIdsubjefatura())){%>
                                <tr>
                                    <td><%=s.getNombre()%></td>
                                    <td><%=s.getDireccion()%></td>
                                    <td><%=s.getResponsable()%></td>
                                    <td class="center-col-lg-1">
                                        <button type="button" class="btn btn-default btn-xs" onclick="javascript:go2to('Escuela/divlistado.jsp?supervision=<%=s.getIdsupervision()%>','page-wrapper')"><i class="fa fa-arrow-circle-down"></i></button>
                                        <button type="button" class="btn btn-warning btn-xs" onclick="javascript:go2to('Supervision/supervision.jsp?id=<%=s.getIdsupervision()%>','page-wrapper')"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <%}%>
                            <tr>
                                <td colspan="3"></td>
                                <td class="center-col-lg-1">
                                    <button type="button" class="btn btn-success btn-xs" onclick="javascript:go2to('Supervision/supervision.jsp?subjefatura=<%=subjefatura.getIdsubjefatura()%>','page-wrapper');"><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                    <div class="col-lg-12 col-center">
                        <ul class="pagination">
                            <%java.util.Collections.reverse(navegaciones);
                            for(auxiliares.Navegacion navegacion:navegaciones){%>
                                <li><a href="#" onclick="javascript:go2to('<%=navegacion.getUrl()%>','page-wrapper');"><%=navegacion.getNombre()%> ></a></li>
                            <%}
                            java.util.Collections.reverse(navegaciones);%>
                            <li><a href="#" onclick="javascript:go2to('Subjefatura/divlistado.jsp','page-wrapper');">SUBJEFATURAS ></a></li>
                        </ul>
                    </div>
                    <!-- /. div-pagination -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<%
facade.close();
sfacade.close();
%>