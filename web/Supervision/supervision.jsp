<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<%@page import="java.io.File"%>
<%@page import="beans.Supervision"%>
<%@page import="auxiliares.Nivel"%>
<jsp:useBean id="supervision" class="beans.Supervision" scope="page"/>
<jsp:setProperty name="supervision" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.SupervisionFacade"/>

<jsp:useBean id="subjefatura" class="beans.Subjefatura" scope="page"/>
<jsp:useBean id="sfacade" scope="page" class="facades.SubjefaturaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarSupervision(supervision);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarSupervision(supervision);        
    }
}
if(request.getParameter("id")!=null){
    int idsupervision=Integer.parseInt((String)request.getParameter("id"));
    supervision=facade.getById(idsupervision);
    subjefatura = sfacade.getById(supervision.getSubjefatura_idsubjefatura());
}else{
    supervision.setIdsupervision(0);
    supervision.setNombre("");
    supervision.setDireccion("");
    supervision.setTelefono("");
    supervision.setCentrotrabajo("");
    supervision.setResponsable("");
    supervision.setZonaescolar("");
    if(request.getParameter("subjefatura")!=null){
        int idsubjefatura = Integer.parseInt(request.getParameter("subjefatura"));
        subjefatura = sfacade.getById(idsubjefatura);
    }
    supervision.setSubjefatura_idsubjefatura(subjefatura.getIdsubjefatura());
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Supervision</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos de la Supervision en la Subjefatura [<%=subjefatura.getNombre()%>]
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="supervision.jsp" method="post">
                        <input type="hidden" id="idsupervision" name="idsupervision" value="<%=supervision.getIdsupervision()%>"/>
                        <input type="hidden" id="subjefatura_idsubjefatura" name="subjefatura_idsubjefatura" value="<%=supervision.getSubjefatura_idsubjefatura()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" maxlength="50" value="<%=supervision.getNombre()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="direccion">Direccion</label>
                                <input class="form-control" id="direccion" name="direccion" type="text" placeholder="Direccion" value="<%=supervision.getDireccion()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="telefono">Telefono</label>
                                <input class="form-control" id="telefono" name="telefono" type="text" placeholder="Telefono" value="<%=supervision.getTelefono()%>" required>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Supervision/supervision.jsp','Supervision/divlistado.jsp?subjefatura=<%=subjefatura.getIdsubjefatura()%>','form1','page-wrapper');">Guardar</button>
                            <%if(supervision.getIdsupervision()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Supervision/supervision.jsp','Supervision/divlistado.jsp?subjefatura=<%=subjefatura.getIdsubjefatura()%>','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Supervision/divlistado.jsp?subjefatura=<%=subjefatura.getIdsubjefatura()%>','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="centrotrabajo">Centro de Trabajo</label>
                                <input class="form-control" id="centrotrabajo" name="centrotrabajo" type="text" placeholder="Centro de Trabajo" value="<%=supervision.getCentrotrabajo()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="responsable">Responsable</label>
                                <input class="form-control" id="responsable" name="responsable" type="text" placeholder="Responsable" value="<%=supervision.getResponsable()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="zonaescolar">Zona Escolar</label>
                                <input class="form-control" id="zonaescolar" name="zonaescolar" type="text" placeholder="Zona Escolar" value="<%=supervision.getZonaescolar()%>" required>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
sfacade.close();
%>