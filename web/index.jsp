<%-- 
    Document   : login
    Created on : Sep 24, 2014, 1:37:05 PM
    Author     : CarlosAlberto
--%>

<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Calendar"%>

<jsp:useBean id="facade" scope="page" class="facades.UsuarioFacade"/>

<%response.setHeader("Cache-Control", "no-cache");
response.setHeader("Cache-Control", "no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);
session.removeAttribute("usuario");

if(request.getParameter("mail")!=null&&request.getParameter("pass")!=null){
    String mail=request.getParameter("mail");
    String pass=request.getParameter("pass");
    beans.Usuario usuario=facade.getByMailPass(mail,pass);
    if(usuario.getIdusuario()!=0){
        session.setAttribute("usuario",usuario);
        response.sendRedirect(request.getContextPath()+"/principal.jsp");
    }
}

boolean compatible=false;
String header=request.getHeader("user-agent");
if(header.contains("Chrome")){
    header="Google Chrome";
    compatible=true;
}else{
    if(header.contains("MSIE")){
        header="MS Explorer";
    }else{
        if(header.contains("Firefox")){
            header="Mozilla Firefox";
        }else{
            header="Navegador Desconocido";
        }
    }
}%>

<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="sistema de consulta de resultados">
        <meta name="author" content="Ing. Carlos Zarate">
        <title>Reporter - Consulta de Resultados</title>
        <link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico">
        <!-- Bootstrap Core CSS -->
        <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <link href="<%=request.getContextPath()%>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<%=request.getContextPath()%>/css/sb-admin-2.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="<%=request.getContextPath()%>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            function login(){
                var str="";
                if(!document.getElementById('mail').validity.valid){str+="Mail No Valido\n";}
                if(!document.getElementById('pass').validity.valid){str+="Password No Valido\n";}
                if(str!==""){
                    alert('Se encontraron los siguientes errores:\n\n'+str);
                }else{
                    document.form1.pass.value=hex_md5(document.form1.pass.value);
                    document.forms['form1'].submit();
                }
            }
            function listener(e){
                var unicode=e.keyCode?e.keyCode:e.charCode;
                if(unicode===13){
                    login();
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><img src="<%=request.getContextPath()%>/images/logo.png"></h3>
                        </div>
                        <div class="panel-body">
                            <%if(compatible){%>
                                <form role="form" name="form1" id="form1" method="post" action="<%=request.getContextPath()%>/index.jsp">
                                    <fieldset>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Mail" name="mail" id="mail" type="email" autofocus>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Password" name="pass" id="pass" type="password" onkeyup="javascript:listener(event);">
                                        </div>
                                        <!-- Change this to a button or input when using this as a form -->
                                        <a href="#" class="btn btn-lg btn-danger btn-block" onclick="javascript:login();">Ingresar</a>
                                    </fieldset>
                                </form>
                            <%}else{%>
                                <div class="form-group">
                                    <label for="Descripcion">No es un navegador compatible.</label>
                                </div> <!-- /login-fields -->
                                <a href="https://drive.google.com/file/d/0BzA0o09sRtZUaC1YZ1ZWRGJYRlE/view?usp=sharing" class="btn btn-danger" download><i class="icon-download-alt icon-white"></i> Descargar Google Chrome</a>
                            <%}%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- javascript MD5 -->
        <script src="<%=request.getContextPath()%>/js/md5.js"></script>
        <!-- jQuery Version 1.11.0 -->
        <script src="<%=request.getContextPath()%>/js/jquery-1.11.0.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="<%=request.getContextPath()%>/js/sb-admin-2.js"></script>
        <script src="<%=request.getContextPath()%>/js/control.js"></script>
        <script src="<%=request.getContextPath()%>/js/plugins/metisMenu/metisMenu.min.js"></script>      
    </body>
</html>

<%
facade.close();
%>