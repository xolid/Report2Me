<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<%@page import="java.io.File"%>
<%@page import="beans.Subjefatura"%>
<%@page import="auxiliares.Nivel"%>
<jsp:useBean id="subjefatura" class="beans.Subjefatura" scope="page"/>
<jsp:setProperty name="subjefatura" property="*" />

<jsp:useBean id="facade" scope="page" class="facades.SubjefaturaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarSubjefatura(subjefatura);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarSubjefatura(subjefatura);        
    }
}
if(request.getParameter("id")!=null){
    int idsubjefatura=Integer.parseInt((String)request.getParameter("id"));
    subjefatura=facade.getById(idsubjefatura);
}else{
    subjefatura.setIdsubjefatura(0);
    subjefatura.setNombre("");
    subjefatura.setDireccion("");
    subjefatura.setTelefono("");
    subjefatura.setCentrotrabajo("");
    subjefatura.setResponsable("");
}
int index=0;
if(request.getParameter("index")!=null){
    index=Integer.parseInt(request.getParameter("index"));
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Subjefatura</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos de la Subjefatura
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="subjefatura.jsp" method="post">
                        <input type="hidden" id="idsubjefatura" name="idsubjefatura" value="<%=subjefatura.getIdsubjefatura()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" maxlength="50" value="<%=subjefatura.getNombre()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="direccion">Direccion</label>
                                <input class="form-control" id="direccion" name="direccion" type="text" placeholder="Direccion" value="<%=subjefatura.getDireccion()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="telefono">Telefono</label>
                                <input class="form-control" id="telefono" name="telefono" type="text" placeholder="Telefono" value="<%=subjefatura.getTelefono()%>" required>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Subjefatura/subjefatura.jsp','Subjefatura/divlistado.jsp?index=<%=index%>','form1','page-wrapper');">Guardar</button>
                            <%if(subjefatura.getIdsubjefatura()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Subjefatura/subjefatura.jsp','Subjefatura/divlistado.jsp?index=<%=index%>','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Subjefatura/divlistado.jsp?index=<%=index%>','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="centrotrabajo">Centro de Trabajo</label>
                                <input class="form-control" id="centrotrabajo" name="centrotrabajo" type="text" placeholder="Centro de Trabajo" value="<%=subjefatura.getCentrotrabajo()%>" required>
                            </div>
                            <div class="form-group">
                                <label for="responsable">Responsable</label>
                                <input class="form-control" id="responsable" name="responsable" type="text" placeholder="Responsable" value="<%=subjefatura.getResponsable()%>" required>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%facade.close();%>