<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="facade" scope="page" class="facades.SubjefaturaFacade"/>

<%
session.removeAttribute("navegaciones");
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Subjefaturas</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Listado de Subjefaturas Registradas
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-5">Nombre</th>
                                <th class="col-lg-6">Direccion</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(beans.Subjefatura s:facade.getAll()){%>
                                <tr>
                                    <td><%=s.getNombre()%></td>
                                    <td><%=s.getDireccion()%></td>
                                    <td class="center-col-lg-1">
                                        <button type="button" class="btn btn-default btn-xs" onclick="javascript:go2to('Supervision/divlistado.jsp?subjefatura=<%=s.getIdsubjefatura()%>','page-wrapper')"><i class="fa fa-arrow-circle-down"></i></button>
                                        <button type="button" class="btn btn-warning btn-xs" onclick="javascript:go2to('Subjefatura/subjefatura.jsp?id=<%=s.getIdsubjefatura()%>','page-wrapper')"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <%}%>
                            <tr>
                                <td colspan="2"></td>
                                <td class="center-col-lg-1">
                                    <button type="button" class="btn btn-success btn-xs" onclick="javascript:go2to('Subjefatura/subjefatura.jsp','page-wrapper');"><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                    <div class="col-lg-12 col-center">
                        <ul class="pagination">
                            <li><a href="#" onclick="javascript:go2to('Subjefatura/divlistado.jsp','page-wrapper');">SUBJEFATURAS ></a></li>
                        </ul>
                    </div>
                    <!-- /. div-pagination -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<%
facade.close();
%>