<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="examen" class="beans.Examen" scope="page"/>
<jsp:useBean id="efacade" scope="page" class="facades.ExamenFacade"/>
<jsp:useBean id="gfacade" scope="page" class="facades.GradoFacade"/>
<jsp:useBean id="mfacade" scope="page" class="facades.MateriaFacade"/>
<jsp:useBean id="facade" scope="page" class="facades.EvaluacionFacade"/>

<%if(request.getParameter("examen")!=null){
    int idexamen=Integer.parseInt(request.getParameter("examen"));
    examen = efacade.getById(idexamen);  
}%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Evaluacion</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Listado de Evaluaciones Registradas de la Examen [<%=examen.getNombre()%>]
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-5">Grado</th>
                                <th class="col-lg-6">Materia</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(beans.Evaluacion e:facade.getByExamen(examen.getIdexamen())){%>
                                <tr>
                                    <td><%=gfacade.getById(e.getGrado_idgrado()).getNombre()%></td>
                                    <td><%=mfacade.getById(e.getMateria_idmateria()).getNombre()%></td>
                                    <td class="center-col-lg-1">
                                        <button type="button" class="btn btn-default btn-xs" onclick="javascript:go2to('Pregunta/divlistado.jsp?evaluacion=<%=e.getIdevaluacion()%>','page-wrapper')"><i class="fa fa-arrow-circle-down"></i></button>
                                        <button type="button" class="btn btn-warning btn-xs" onclick="javascript:go2to('Evaluacion/evaluacion.jsp?id=<%=e.getIdevaluacion()%>','page-wrapper')"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <%}%>
                            <tr>
                                <td colspan="2"></td>
                                <td class="center-col-lg-1">
                                    <button type="button" class="btn btn-success btn-xs" onclick="javascript:go2to('Evaluacion/evaluacion.jsp?examen=<%=examen.getIdexamen()%>','page-wrapper');"><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                    <div class="col-lg-12 col-center">
                        <ul class="pagination">
                            <li><a href="#" onclick="javascript:go2to('Examen/divlistado.jsp','page-wrapper');">Volver</a></li>
                        </ul>
                    </div>
                    <!-- /. div-pagination -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<%
facade.close();
efacade.close();
gfacade.close();
mfacade.close();
%>