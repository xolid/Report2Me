<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<jsp:useBean id="evaluacion" class="beans.Evaluacion" scope="page"/>
<jsp:setProperty name="evaluacion" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.EvaluacionFacade"/>

<jsp:useBean id="examen" class="beans.Examen" scope="page"/>
<jsp:useBean id="efacade" scope="page" class="facades.ExamenFacade"/>
<jsp:useBean id="gfacade" scope="page" class="facades.GradoFacade"/>
<jsp:useBean id="mfacade" scope="page" class="facades.MateriaFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarEvaluacion(evaluacion);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarEvaluacion(evaluacion);        
    }
}
if(request.getParameter("id")!=null){
    int idevaluacion=Integer.parseInt((String)request.getParameter("id"));
    evaluacion=facade.getById(idevaluacion);
    examen = efacade.getById(evaluacion.getExamen_idexamen());
}else{
    evaluacion.setIdevaluacion(0);
    evaluacion.setGrado_idgrado(0);
    evaluacion.setMateria_idmateria(0);
    if(request.getParameter("examen")!=null){
        int idexamen = Integer.parseInt(request.getParameter("examen"));
        examen = efacade.getById(idexamen);
    }
    evaluacion.setExamen_idexamen(examen.getIdexamen());
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Evaluacion</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos de la Evaluacion en la Examen [<%=examen.getNombre()%>]
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="evaluacion.jsp" method="post">
                        <input type="hidden" id="idevaluacion" name="idevaluacion" value="<%=evaluacion.getIdevaluacion()%>"/>
                        <input type="hidden" id="examen_idexamen" name="examen_idexamen" value="<%=evaluacion.getExamen_idexamen()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="grado_idgrado">Grado:</label>
                                <select class="form-control" id="grado_idgrado" name="grado_idgrado">
                                    <%for(beans.Grado grado:gfacade.getAll()){%>
                                        <option value="<%=grado.getIdgrado()%>" <%=grado.getIdgrado()==evaluacion.getGrado_idgrado()?"SELECTED":""%>><%=grado.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Evaluacion/evaluacion.jsp','Evaluacion/divlistado.jsp?examen=<%=examen.getIdexamen()%>','form1','page-wrapper');">Guardar</button>
                            <%if(evaluacion.getIdevaluacion()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Evaluacion/evaluacion.jsp','Evaluacion/divlistado.jsp?examen=<%=examen.getIdexamen()%>','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Evaluacion/divlistado.jsp?examen=<%=examen.getIdexamen()%>','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="materia_idmateria">Materia:</label>
                                <select class="form-control" id="materia_idmateria" name="materia_idmateria">
                                    <%for(beans.Materia materia:mfacade.getAll()){%>
                                        <option value="<%=materia.getIdmateria()%>" <%=materia.getIdmateria()==evaluacion.getMateria_idmateria()?"SELECTED":""%>><%=materia.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
efacade.close();
%>