<%-- 
    Document   : bloques
    Created on : 2/02/2015, 04:10:39 PM
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.InscritoFacade"/>

<%
int idexamen = 0;
if(request.getParameter("idexamen")!=null){
    idexamen = Integer.parseInt(request.getParameter("idexamen"));
}

java.text.DecimalFormat decimalFormat = new java.text.DecimalFormat();
decimalFormat.setMaximumFractionDigits(2);

String total = "ND";
String promedio = "ND";
String aciertos = "ND";
String reactivos = "ND";

%>

<div class="col-lg-3 col-md-6">
    <div class="panel panel-green">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-check-circle-o fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">
                        <%=aciertos%>
                    </div>
                    <div>Aciertos Totales</div>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer">
                <span class="pull-left">Ver Detalles</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="panel panel-yellow">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-check-circle fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">
                        <%=reactivos%>
                    </div>
                    <div>Reactivos Evaluados</div>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer">
                <span class="pull-left">Ver Detalles</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-dot-circle-o fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge"><%=promedio%></div>
                    <div>Promedio de Calificaciones</div>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer">
                <span class="pull-left">Ver Detalles</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>
<div class="col-lg-3 col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3">
                    <i class="fa fa-check-square-o fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge"><%=total%></div>
                    <div>Total de Alumnos Inscritos</div>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer">
                <span class="pull-left">Ver Detalles</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>

<%
facade.close();
%>