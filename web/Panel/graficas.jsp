<%-- 
    Document   : graficas
    Created on : 2/02/2015, 05:56:46 PM
    Author     : solid
--%>

<jsp:useBean id="facade" scope="page" class="facades.InscritoFacade"/>
<jsp:useBean id="efacade" scope="page" class="facades.EscuelaFacade"/>

<%
int idciclo = 0;
if(request.getParameter("idciclo")!=null){
    idciclo = Integer.parseInt(request.getParameter("idciclo"));
}

java.text.DecimalFormat decimalFormat = new java.text.DecimalFormat();
decimalFormat.setMaximumFractionDigits(2);

java.util.List<String> nombres = new java.util.ArrayList();
java.util.List<Object> valores = new java.util.ArrayList();
java.util.List<Object> promedios = new java.util.ArrayList();

String nombrescsv = "\""+nombres.toString().replace("[","").replace("]","").replace(", ",",").replace(",","\",\"")+"\"";
String valorescsv = valores.toString().replace("[","").replace("]","").replace(", ",",");
String promedioscsv = promedios.toString().replace("[","").replace("]","").replace(", ",",").replace("null","0");

%>

<div class="col-lg-8">
    <div class="panel panel-red">
        <div class="panel-heading">
            <i class="fa fa-bar-chart-o fa-fw"></i> Estudiantes Inscritos por Escuela en el Ciclo Seleccionado
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="center-col-lg-1">
                <canvas id="inscritos_escuela_chart"></canvas>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
<div class="col-lg-4">
    <!-- /.panel -->
    <div class="panel panel-red">
        <div class="panel-heading">
            <i class="fa fa-dot-circle-o fa-fw"></i> Promedio de la Escuela en el Ciclo Seleccionado
        </div>
        <div class="panel-body">
            <div class="center-col-lg-1">
                <canvas id="promedio_escuela_chart"></canvas>
            </div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-4 -->

<script>
    function aleatorio(inferior,superior){ 
        numPosibilidades = superior - inferior;
        aleat = Math.random() * numPosibilidades;
        aleat = Math.floor(aleat);
        return parseInt(inferior) + aleat;
     }
    function randomColor(){ 
        var hexadecimal = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
        var color_aleatorio = "#"; 
        for (i=0;i<6;i++){ 
           var posarray = aleatorio(0,hexadecimal.length);
           color_aleatorio += hexadecimal[posarray]; 
        } 
        return color_aleatorio;
    }
    
    var barChartData = {
        labels : [
            "UNO","DOS"
        ],
        datasets : [
            {
                fillColor : "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data : [1,2]
            }
        ]
    };
    var ctx1 = document.getElementById("inscritos_escuela_chart").getContext("2d");
    window.myBar = new Chart(ctx1).Bar(barChartData, {responsive : true});
    
    
    
    var polarData = [
        <%int index = 0;
        for(String nombre:nombres){
            out.print("{value: "+promedios.get(index++)+",color: randomColor(),label: \""+nombre+"\"},");
        }%>
    ];
    var ctx = document.getElementById("promedio_escuela_chart").getContext("2d");
    window.myPolarArea = new Chart(ctx).Pie(polarData, {responsive:true});
</script>
