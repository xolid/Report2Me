<%-- 
    Document   : inscribir
    Created on : 2/02/2015, 09:19:11 AM
    Author     : solid
--%>

<jsp:useBean id="alumno" class="beans.Alumno" scope="page"/>
<jsp:useBean id="facade" scope="page" class="facades.AlumnoFacade"/>

<%
if(request.getParameter("curp")!=null){
    String curp = request.getParameter("curp");
    alumno = facade.getByCurp(curp);
    if(alumno.getIdalumno()==0){
        alumno.setIdalumno(0);
        alumno.setCurp("");
        alumno.setPaterno("");
        alumno.setMaterno("");
        alumno.setNombre("");
        alumno.setFnacimiento("");
    }
}else{
    alumno.setIdalumno(0);
    alumno.setCurp("");
    alumno.setPaterno("");
    alumno.setMaterno("");
    alumno.setNombre("");
    alumno.setFnacimiento("");
}
%>

<div class="row">
    <form role="form" id="formsearch" name="formsearch" action="inscribir.jsp" method="post">
        <input type="hidden" id="idalumno" name="idalumno" value="<%=alumno.getIdalumno()%>"/>
        <div class="col-lg-2">
            <div class="form-group">
                <label for="curp">C.U.R.P.</label>
                <input class="form-control" id="curp" name="curp" type="text" placeholder="C.U.R.P." value="<%=alumno.getCurp()%>" autofocus>
            </div>
            <button class="btn btn-default" type="button" onclick="javascript:search('Alumno/buscar2.jsp','formsearch','buscar');">Buscar</button>
            <%if(alumno.getIdalumno()!=0){%>
                <button class="btn btn-warning" type="button" onclick="go2to('Alumno/alumno2.jsp?id=<%=alumno.getIdalumno()%>','page-wrapper')">Editar</button>
            <%}%>
        </div>
        <!-- /.col-lg-6 (nested) -->
        <div class="col-lg-2">
            <div class="form-group">
                <label for="paterno">A. Paterno</label>
                <input class="form-control" id="paterno" name="paterno" type="text" placeholder="A. Paterno" value="<%=alumno.getPaterno()%>" disabled>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label for="materno">A. Materno</label>
                <input class="form-control" id="materno" name="materno" type="text" placeholder="A. Materno" value="<%=alumno.getMaterno()%>" disabled>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" value="<%=alumno.getNombre()%>" disabled>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label for="fnacimiento">F. Nacimiento</label>
                <input class="form-control" id="fnacimiento" name="fnacimiento" type="text" placeholder="F. Nacimiento" value="<%=alumno.getFnacimiento()%>" disabled>
            </div>
        </div>
        <!-- /.col-lg-6 (nested) -->
    </form>                
    <!-- /.form -->    
</div>
<!-- /.row (nested) -->