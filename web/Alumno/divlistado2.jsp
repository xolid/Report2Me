<%-- 
    Document   : divlistado
    Created on : May 19, 2012, 5:06:59 PM
    Author     : czarate
--%>

<jsp:useBean id="facade" scope="page" class="facades.AlumnoFacade"/>

<%
int index=0;
if(request.getParameter("index")!=null){
    index=Integer.parseInt(request.getParameter("index"));
}
String campo="paterno";
if(request.getParameter("campo")!=null){
    campo=request.getParameter("campo");
}
int size=10;
long total=(Long)facade.getNoInscritosTotal();
%>

<script>
    function update(){
        try{
            go2to('Alumno/buscar2.jsp','buscar');
        }catch(e){
            close('buscar');
        }
    }
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Alumno Pendientes de Inscripcion</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="form-group">
                    <label for="orden">Ordenar por:</label>
                    <select class="form-control" id="ordenador" onchange="javascript:go2to('Alumno/divlistado2.jsp?index=<%=index%>&campo='+document.getElementById('ordenador').options[document.getElementById('ordenador').selectedIndex].value,'page-wrapper');">
                        <option value="paterno" <%=campo.equals("paterno")?"selected":""%>>A. Paterno</option>
                        <option value="materno" <%=campo.equals("materno")?"selected":""%>>A. Materno</option>
                        <option value="nombre" <%=campo.equals("nombre")?"selected":""%>>Nombre</option>
                        <option value="curp" <%=campo.equals("curp")?"selected":""%>>C.U.R.P.</option>
                    </select>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-2">C.U.R.P.</th>
                                <th class="col-lg-3">Paterno</th>
                                <th class="col-lg-3">Materno</th>
                                <th class="col-lg-3">Nombre(s)</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%for(beans.Alumno s:facade.getNoInscritosLimitedOrdered(index, size, campo)){%>
                                <tr>
                                    <td><%=s.getCurp()%></td>
                                    <td><%=s.getPaterno()%></td>
                                    <td><%=s.getMaterno()%></td>
                                    <td><%=s.getNombre()%></td>
                                    <td class="center-col-lg-1">
                                        <button type="button" class="btn btn-warning btn-xs" onclick="javascript:go2to('Alumno/alumno2.jsp?id=<%=s.getIdalumno()%>&index=<%=index%>&campo='+document.getElementById('ordenador').options[document.getElementById('ordenador').selectedIndex].value,'page-wrapper')"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <%}%>
                            <tr>
                                <td colspan="4"</td>
                                <td class="center-col-lg-1">
                                    <button type="button" class="btn btn-danger btn-xs" onclick="javascript:erase('Alumno/borrarTodos.jsp','Alumno/divlistado2.jsp','formsearch','page-wrapper');">BORRAR TODOS</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                    <div class="col-lg-12 col-center">
                        <ul class="pagination">
                            <%if(index+size<=total){%>
                                <li class="next"><a href="#" onclick="javascript:go2to('Alumno/divlistado2.jsp?index=<%=index+size%>&campo='+document.getElementById('ordenador').options[document.getElementById('ordenador').selectedIndex].value,'page-wrapper');">>></a></li>
                            <%}else{%>
                                <li class="next disabled"><a>>></a></li>
                            <%}%>
                            <li><a>Pagina <%=(index/size+1)+" de "+(total/size+1)%></a></li>
                            <%if(index-size>=0){%>
                                <li class="prev"><a href="#" onclick="javascript:go2to('Alumno/divlistado2.jsp?index=<%=index-size%>&campo='+document.getElementById('ordenador').options[document.getElementById('ordenador').selectedIndex].value,'page-wrapper');"><<</a></li>
                            <%}else{%>
                                <li class="prev disabled"><a href="#"><<</a></li>
                            <%}%>
                        </ul>
                    </div>
                    <!-- /. div-pagination -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
        <div class="panel panel-red">
            <div class="panel-heading">
                Buscar Alumno
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body" id="buscar"></div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
                        
<script>
    update();
</script>

<%
facade.close();
%>