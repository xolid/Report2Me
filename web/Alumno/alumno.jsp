<%-- 
    Document   : prueba
    Created on : Sep 19, 2013, 7:51:39 PM
    Author     : czarate
--%>

<jsp:useBean id="alumno" class="beans.Alumno" scope="page"/>
<jsp:setProperty name="alumno" property="*" />
<jsp:useBean id="facade" scope="page" class="facades.AlumnoFacade"/>

<%if(request.getParameter("accion")!=null){
    if(request.getParameter("accion").equals("1")){
        facade.salvarAlumno(alumno);
    }
    if(request.getParameter("accion").equals("2")){
        facade.borrarAlumno(alumno);        
    }
}
if(request.getParameter("id")!=null){
    int idalumno=Integer.parseInt((String)request.getParameter("id"));
    alumno=facade.getById(idalumno);
}else{
    alumno.setIdalumno(0);
    alumno.setCurp("");
    alumno.setPaterno("");
    alumno.setMaterno("");
    alumno.setNombre("");
    alumno.setFnacimiento("");
}
int index=0;
if(request.getParameter("index")!=null){
    index=Integer.parseInt(request.getParameter("index"));
}
String campo="nombre";
if(request.getParameter("campo")!=null){
    campo=request.getParameter("campo");
}
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Alumno</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Datos del Alumno
            </div>
            <div class="panel-body">
                <div class="row">
                    <form role="form" id="form1" name="form1" action="alumno.jsp" method="post">
                        <input type="hidden" id="idalumno" name="idalumno" value="<%=alumno.getIdalumno()%>"/>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="curp">C.U.R.P.</label>
                                <input class="form-control" id="curp" name="curp" type="text" placeholder="C.U.R.P." maxlength="25" value="<%=alumno.getCurp()%>" pattern="[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$" autofocus required tabindex="1">
                            </div>
                            <div class="form-group">
                                <label for="materno">A. Materno</label>
                                <input class="form-control" id="materno" name="materno" type="text" placeholder="Apellido Materno" value="<%=alumno.getMaterno()%>" required tabindex="3">
                            </div>
                            <div class="form-group">
                                <label>Fecha de Nacimiento</label>
                                <input class="form-control" id="fnacimiento" name="fnacimiento" type="date" value="<%=alumno.getFnacimiento()%>" required tabindex="5">
                            </div>
                            <button class="btn btn-success" type="button" onclick="javascript:save('Alumno/alumno.jsp','Alumno/divlistado.jsp?index=<%=index%>&campo=<%=campo%>','form1','page-wrapper');">Guardar</button>
                            <%if(alumno.getIdalumno()!=0){%>
                                <button class="btn btn-danger" type="button" onclick="javascript:erase('Alumno/alumno.jsp','Alumno/divlistado.jsp?index=<%=index%>&campo=<%=campo%>','form1','page-wrapper');">Borrar</button>
                            <%}%>
                            <button class="btn btn-default" type="button" onclick="javascript:go2to('Alumno/divlistado.jsp?index=<%=index%>&campo=<%=campo%>','page-wrapper');">Volver</button>                            
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="paterno">A. Paterno</label>
                                <input class="form-control" id="paterno" name="paterno" type="text" placeholder="Apellido Paterno" value="<%=alumno.getPaterno()%>" required tabindex="2">
                            </div>
                            <div class="form-group">
                                <label for="nombre">Nombre(s)</label>
                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre" value="<%=alumno.getNombre()%>" required tabindex="4">
                            </div>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </form>                
                    <!-- /.form -->    
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>
<!-- /.row -->

<%
facade.close();
%>