<%-- 
    Document   : InscritosXExamenVista
    Created on : 22/02/2015, 11:01:35 AM
    Author     : solid
--%>

<jsp:useBean id="examenfacade" scope="page" class="facades.ExamenFacade"/>
<jsp:useBean id="escuelafacade" scope="page" class="facades.EscuelaFacade"/>

<%
java.util.List<beans.Examen> examenes = examenfacade.getAll();
java.util.List<beans.Escuela> escuelas = escuelafacade.getAll();
%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Reportes</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-red">
            <div class="panel-heading">
                Listado de Reportes Disponibles
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th class="col-lg-6">Nombre</th>
                                <th class="col-lg-2">Parametro1</th>
                                <th class="col-lg-3">Parametro2</th>
                                <th class="center-col-lg-1">Comandos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Reporte de Estatus de Avance de Escuelas por Examen</td>
                                <td colspan="2">
                                    <select class="form-control" id="examen_idexamen0">
                                        <%for(beans.Examen examen:examenes){%>
                                            <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td class="center-col-lg-2">
                                    <button type="button" class="btn btn-warning btn-xs" onclick="javascript:window.open('Reporte/EstatusXExamen.jsp?idexamen='+document.getElementById('examen_idexamen0').options[document.getElementById('examen_idexamen0').selectedIndex].value);">EJECUTAR</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Reporte de Alumnos Inscritos por Examen</td>
                                <td colspan="2">
                                    <select class="form-control" id="examen_idexamen1">
                                        <%for(beans.Examen examen:examenes){%>
                                            <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td class="center-col-lg-2">
                                    <button type="button" class="btn btn-warning btn-xs" onclick="javascript:window.open('Reporte/InscritosXExamen.jsp?idexamen='+document.getElementById('examen_idexamen1').options[document.getElementById('examen_idexamen1').selectedIndex].value);">EJECUTAR</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Reporte de Calificaciones para Alumnos Inscritos por Examen</td>
                                <td colspan="2">
                                    <select class="form-control" id="examen_idexamen2">
                                        <%for(beans.Examen examen:examenes){%>
                                            <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td class="center-col-lg-2">
                                    <button type="button" class="btn btn-warning btn-xs" onclick="javascript:window.open('Reporte/InscritosXExamenResultados.jsp?idexamen='+document.getElementById('examen_idexamen2').options[document.getElementById('examen_idexamen2').selectedIndex].value);">EJECUTAR</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Reporte de Calificaciones para Alumnos Inscritos por Examen con Detalle por Materia</td>
                                <td>
                                    <select class="form-control" id="examen_idexamen3">
                                        <%for(beans.Examen examen:examenes){%>
                                            <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" id="escuela_idescuela">
                                        <%for(beans.Escuela escuela:escuelas){%>
                                            <option value="<%=escuela.getIdescuela()%>"><%=escuela.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td class="center-col-lg-2">
                                    <button type="button" class="btn btn-warning btn-xs" onclick="javascript:window.open('Reporte/InscritosXExamenResultadosXMateria.jsp?idexamen='+document.getElementById('examen_idexamen3').options[document.getElementById('examen_idexamen3').selectedIndex].value+'&idescuela='+document.getElementById('escuela_idescuela').options[document.getElementById('escuela_idescuela').selectedIndex].value);">EJECUTAR</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Reporte de Calificaciones para Alumnos Inscritos por Examen con Detalle por Pregunta</td>
                                <td>
                                    <select class="form-control" id="examen_idexamen4">
                                        <%for(beans.Examen examen:examenes){%>
                                            <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" id="escuela_idescuela1">
                                        <%for(beans.Escuela escuela:escuelas){%>
                                            <option value="<%=escuela.getIdescuela()%>"><%=escuela.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td class="center-col-lg-2">
                                    <button type="button" class="btn btn-warning btn-xs" onclick="javascript:window.open('Reporte/InscritosXExamenResultadosXPregunta.jsp?idexamen='+document.getElementById('examen_idexamen4').options[document.getElementById('examen_idexamen4').selectedIndex].value+'&idescuela='+document.getElementById('escuela_idescuela1').options[document.getElementById('escuela_idescuela1').selectedIndex].value);">EJECUTAR</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Reporte de Promedio por Grupos por Examen</td>
                                <td>
                                    <select class="form-control" id="examen_idexamen5">
                                        <%for(beans.Examen examen:examenes){%>
                                            <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" id="escuela_idescuela2">
                                        <%for(beans.Escuela escuela:escuelas){%>
                                            <option value="<%=escuela.getIdescuela()%>"><%=escuela.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td class="center-col-lg-2">
                                    <button type="button" class="btn btn-warning btn-xs" onclick="javascript:window.open('Reporte/PromedioXGrupoMateria.jsp?idexamen='+document.getElementById('examen_idexamen5').options[document.getElementById('examen_idexamen5').selectedIndex].value+'&idescuela='+document.getElementById('escuela_idescuela2').options[document.getElementById('escuela_idescuela2').selectedIndex].value);">EJECUTAR</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Reporte de Promedio por Grados por Examen</td>
                                <td>
                                    <select class="form-control" id="examen_idexamen6">
                                        <%for(beans.Examen examen:examenes){%>
                                            <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" id="escuela_idescuela3">
                                        <%for(beans.Escuela escuela:escuelas){%>
                                            <option value="<%=escuela.getIdescuela()%>"><%=escuela.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td class="center-col-lg-2">
                                    <button type="button" class="btn btn-warning btn-xs" onclick="javascript:window.open('Reporte/PromedioXGradoMateria.jsp?idexamen='+document.getElementById('examen_idexamen6').options[document.getElementById('examen_idexamen6').selectedIndex].value+'&idescuela='+document.getElementById('escuela_idescuela3').options[document.getElementById('escuela_idescuela3').selectedIndex].value);">EJECUTAR</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Reporte de Promedio por Escuela General por Examen</td>
                                <td>
                                    <select class="form-control" id="examen_idexamen7">
                                        <%for(beans.Examen examen:examenes){%>
                                            <option value="<%=examen.getIdexamen()%>"><%=examen.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control" id="escuela_idescuela4">
                                        <%for(beans.Escuela escuela:escuelas){%>
                                            <option value="<%=escuela.getIdescuela()%>"><%=escuela.getNombre()%></option>
                                        <%}%>
                                    </select>
                                </td>
                                <td class="center-col-lg-2">
                                    <button type="button" class="btn btn-warning btn-xs" onclick="javascript:window.open('Reporte/PromedioXEscuelaMateria.jsp?idexamen='+document.getElementById('examen_idexamen7').options[document.getElementById('examen_idexamen7').selectedIndex].value+'&idescuela='+document.getElementById('escuela_idescuela4').options[document.getElementById('escuela_idescuela4').selectedIndex].value);">EJECUTAR</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /. div-filters -->
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
                                    
<%
examenfacade.close();
%>