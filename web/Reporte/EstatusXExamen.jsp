<%-- 
    Document   : Inscritos
    Created on : 22/02/2015, 09:42:24 AM
    Author     : solid
--%>

<%
if(request.getParameter("idexamen")!=null){
    tools.DataCon con = new tools.DataCon();
    java.io.File reportFile = new java.io.File(application.getRealPath("/Reporte/EstatusXExamen.jasper"));

    java.util.Map parameters = new java.util.HashMap();
    parameters.put("idexamen",Integer.parseInt(request.getParameter("idexamen")));
    byte[] bytes = net.sf.jasperreports.engine.JasperRunManager.runReportToPdf(reportFile.getPath(),parameters,con.getConnection());

    response.addHeader("Content-Disposition","attachment;filename=EstatusXExamen.pdf");
    response.setContentType("application/pdf");
    response.setContentLength(bytes.length);
    ServletOutputStream ouputStream = response.getOutputStream();
    ouputStream.write(bytes,0,bytes.length);
    ouputStream.flush();
    ouputStream.close();
    con.closeConnection();
}%>

<script>
    window.close();
</script>