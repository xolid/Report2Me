/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function go2to(url, div) {
    xmlhttpPost2(url, div, '');
}

function gotoEXEC(url, div, fun) {
    xmlhttpPostEXEC(url, div, '', fun);
}

function xmlhttpPost2(strURL, strDIV, strPAR) {
    var self = this;
    if (window.XMLHttpRequest) {
        self.xmlHttpReq = new XMLHttpRequest();
    }
    else {
        if (window.ActiveXObject) {
            self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    self.xmlHttpReq.open('POST', strURL, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    self.xmlHttpReq.onreadystatechange = function() {
        if (self.xmlHttpReq.readyState === 4) {
            if(self.xmlHttpReq.status === 404){
                document.getElementById(strDIV).innerHTML = '<div class="row"><div class="col-lg-12"><h1 class="page-header">Pagina bajo construccion</h1></div><!-- /.col-lg-12 --></div><!-- /.row -->';
            }else{
                var scs=self.xmlHttpReq.responseText.extractScript();
                document.getElementById(strDIV).innerHTML=self.xmlHttpReq.responseText;
                scs.evalScript();
            }
        } else {
            document.getElementById(strDIV).innerHTML = '<div><img src="/Report2Me/images/loading.gif"/></div>';
        }
    };
    self.xmlHttpReq.send(strPAR);
}

function xmlhttpPostEXEC(strURL, strDIV, strPAR, strFUN) {
    var self = this;
    if (window.XMLHttpRequest) {
        self.xmlHttpReq = new XMLHttpRequest();
    }
    else {
        if (window.ActiveXObject) {
            self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    self.xmlHttpReq.open('POST', strURL, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    self.xmlHttpReq.onreadystatechange = function() {
        if (self.xmlHttpReq.readyState === 4) {
            if(self.xmlHttpReq.status === 404){
                document.getElementById(strDIV).innerHTML = '<div class="row"><div class="col-lg-12"><h1 class="page-header">Pagina bajo construccion</h1></div><!-- /.col-lg-12 --></div><!-- /.row -->';
            }else{
                document.getElementById(strDIV).innerHTML = self.xmlHttpReq.responseText;
                window.setTimeout(strFUN, 100);
            }
        } else {
            document.getElementById(strDIV).innerHTML = '<div><img src="/Report2Me/images/loading.gif"/></div>';
        }
    };
    self.xmlHttpReq.send(strPAR);
}

function fileUpload(form, action, div, execute) {
    var iframe = document.createElement("iframe");
    iframe.setAttribute("id", "upload_iframe");
    iframe.setAttribute("name", "upload_iframe");
    iframe.setAttribute("width", "0");
    iframe.setAttribute("height", "0");
    iframe.setAttribute("border", "0");
    iframe.setAttribute("style", "width: 0; height: 0; border: none;");
    form.parentNode.appendChild(iframe);
    window.frames['upload_iframe'].name = "upload_iframe";
    iframeId = document.getElementById("upload_iframe");
    var eventHandler = function () {
        if (iframeId.detachEvent){ 
            iframeId.detachEvent("onload", eventHandler);
        }else{ 
            iframeId.removeEventListener("load", eventHandler, false);
        }
        if (iframeId.contentDocument) {
            content = iframeId.contentDocument.body.innerHTML;
        }else{ 
            if (iframeId.contentWindow) {
                content = iframeId.contentWindow.document.body.innerHTML;
            } else{ 
                if (iframeId.document) {
                    content = iframeId.document.body.innerHTML;
                }
            }
        }
        document.getElementById(div).innerHTML = content;
        setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
        setTimeout(execute, 1000);
    };
    if (iframeId.addEventListener) 
        iframeId.addEventListener("load", eventHandler, true);
    if (iframeId.attachEvent) 
        iframeId.attachEvent("onload", eventHandler);
    form.setAttribute("target", "upload_iframe");
    form.setAttribute("action", action);
    form.setAttribute("method", "post");
    form.setAttribute("enctype", "multipart/form-data");
    form.setAttribute("encoding", "multipart/form-data");
    form.submit();
    document.getElementById(div).innerHTML = '<div><img src="/Report2Me/images/loading.gif"/></div>';
}