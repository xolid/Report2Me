/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function save(url, urlRedirect, formName, div) {
    var str = validInputs(formName);
    if (str === "") {
        if (confirm('Confirma desea salvar?')) {
            xmlhttpPostEXEC(url, div, 'accion=1&' + inputsToParams(formName), 'xmlhttpPost2(\'' + urlRedirect + '\',\'' + div + '\',\'\')');
        }
    } else {
        alert('Se encontraron los siguientes errores:\n\n' + str);
    }
}

function erase(url, urlRedirect, formName, div) {
    if (confirm('Confirma desea borrar?')) {
        xmlhttpPostEXEC(url, div, 'accion=2&' + inputsToParams(formName), 'xmlhttpPost2(\'' + urlRedirect + '\',\'' + div + '\',\'\')');
    }
}

function search(url, formName, div) {
    xmlhttpPostEXEC(url, div, inputsToParams(formName), '');
}

function close(strDIV) {
    document.getElementById(strDIV).innerHTML = '';
}

function executeRedirec(url, params, urlRedirect, div) {
    xmlhttpPostEXEC(url, div, params, 'xmlhttpPost2(\'' + urlRedirect + '\',\'' + div + '\',\'\')');
}

function confirmExecution(url, params, strMessage, div) {
    if (confirm(strMessage)) {
        xmlhttpPost(url, div, params);
    }
}

function clean(formName) {
    restartInputs(formName);
}

function restartInputs(formName) {
    var elem = document.getElementById(formName).elements;
    for (var i = 0; i < elem.length; i++) {
        var inputTagName = elem[i].tagName;
        if (inputTagName === 'INPUT') {
            document.getElementById(elem[i].id).value = "";
        }
        if (inputTagName === 'SELECT') {
            document.getElementById(elem[i].id);
        }
        if (inputTagName === 'TEXTAREA') {
            document.getElementById(elem[i].id).value = "";
        }
    }
}

function inputsToParams(formName) {
    var string = '';
    var elem = document.getElementById(formName).elements;
    for (var i = 0; i < elem.length; i++) {
        var inputTagName = elem[i].tagName;
        if (inputTagName === 'INPUT' || inputTagName === 'SELECT' || inputTagName === 'TEXTAREA') {
            var inputName = elem[i].name;
            var inputValue = '';
            switch (elem[i].type) {
                case 'file':
                    inputValue = elem[i].value.replace("C:\\fakepath\\", "");
                    break;
                case 'select-multiple':
                    inputValue = getSelectedItems(elem[i]);
                    break;
                case 'datetime-local':
                    inputValue = elem[i].value.replace("T", " ");
                    break;
                case 'radio':
                    inputValue = getRadioCheckedValue(formName,inputName);
                    break;
                default:
                    inputValue = elem[i].value;
                    break;
            }
            if (string === "") {
                string = inputName + "=" + inputValue;
            } else {
                string = string + "&" + inputName + "=" + inputValue;
            }
        }
    }
    return string;
}

function getRadioCheckedValue(formName,radio){
    var oRadio = document.getElementById(formName).elements[radio];
    for(var i = 0; i < oRadio.length; i++){
        if(oRadio[i].checked){
            return oRadio[i].value;
        }
    }
   return '';
}

function validInputs(formName) {
    var string = '';
    var elem = document.getElementById(formName).elements;
    for (var i = 0; i < elem.length; i++) {
        var inputTagName = elem[i].tagName;
        if (inputTagName === 'INPUT') {
            if (!elem[i].validity.valid) {
                string += "* " + elem[i].name + " no valido.\n";
            }
        }
        if (inputTagName === 'SELECT') {
            if (elem[i].type === 'select-multiple') {
                if (getSelectedItemsNumber(elem[i]) <= 0 && elem[i].required) {
                    string += "* " + elem[i].name + " debe seleccionar almenos un elemento.\n";
                }
            } else {
                if (elem[i].options.length === 0) {
                    string += "* " + elem[i].name + " no contiene elementos.\n";
                }
            }
        }
        if (inputTagName === 'TEXTAREA') {
            if (elem[i].value === "" && elem[i].required) {
                string += "* " + elem[i].name + " esta vacio.\n";
            }
        }
    }
    return string;
}

function getSelectedItems(element) {
    var selectedList = "";
    for (var i = 0; i < element.options.length; i++) {
        if (element.options[i].selected) {
            if (selectedList === "") {
                selectedList += element.options[i].value;
            } else {
                selectedList += "," + element.options[i].value;
            }
        }
    }
    return selectedList;
}

function getSelectedItemsNumber(element) {
    var number = 0;
    for (var i = 0; i < element.options.length; i++) {
        if (element.options[i].selected) {
            number++;
        }
    }
    return number;
}